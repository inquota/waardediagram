<?php
/**
 * Application UserController
 *
 *
 * @package       app.AppUserController
 * @since         
 */

App::uses('UsersController', 'Users.Controller');

/**
 * AppUserController
 *
 * Extending the Users Plugin.
 *
 * @package		app.AppUserController
 */
class AppUsersController extends UsersController 
{    
	/**
	 * name
	 *
	 * @var string
	 */
	public $name = 'AppUsers';
	
	/**
	 * beforeFilter.
	 *
	 * Extend the Users.Controller, load the AppUser model,
	 * which extends the Users.Model.
	 */
	public function beforeFilter() 
	{
	    parent::beforeFilter();
	    $this->User = ClassRegistry::init('AppUser');
	    $this->set('model', 'AppUser');
	}
	
	public function index()
	{
		$user = $this->{$this->modelClass}->read(null, $this->Auth->user('id'));
		$this->set('user', $user);
	}
	
	/**
 * User register action
 *
 * @return void
 */
	public function add() {
		if ($this->Auth->user()) {
			$this->Session->setFlash(__('U heeft al een account en bent al ingelogd'), 'good');
			$this->redirect('/');
		}

		if (!empty($this->request->data)) {
			$user = $this->{$this->modelClass}->register($this->request->data);
			if ($user !== false) {
				$Event = new CakeEvent(
					'Users.Controller.Users.afterRegistration',
					$this,
					array(
						'data' => $this->request->data,
					)
				);
				$this->getEventManager()->dispatch($Event);
				if ($Event->isStopped()) {
					$this->redirect(array('action' => 'login'));
				}

				 // Auto login
			    if ($this->Auth->login()) {
			    	$user=$this->Auth->user();
			      	$this->Session->setFlash(sprintf(__('Welkom %s, wij hebben een account voor u aangemaakt. Klik op de knop start om direct aan de slag te gaan met onze Marktpositie Analyse Tool!'), $user['hotel']), 'good');
					$this->redirect(array('action' => 'signedup'));
			    }

			} else {
				unset($this->request->data[$this->modelClass]['password']);
				unset($this->request->data[$this->modelClass]['temppassword']);
				$this->Session->setFlash(__('Helaas, wij kunnen geen account voor u aanmaken. Probeer het nogmaals'), 'bad');
			}
		}
	}

/**
 * Common login action
 *
 * @return void
 */
	public function login() {
		$Event = new CakeEvent(
			'Users.Controller.Users.beforeLogin',
			$this,
			array(
				'data' => $this->request->data,
			)
		);

		$this->getEventManager()->dispatch($Event);

		if ($Event->isStopped()) {
			return;
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$Event = new CakeEvent(
					'Users.Controller.Users.afterLogin',
					$this,
					array(
						'data' => $this->request->data,
						'isFirstLogin' => !$this->Auth->user('last_login')
					)
				);

				$this->getEventManager()->dispatch($Event);

				$this->{$this->modelClass}->id = $this->Auth->user('id');
				$this->{$this->modelClass}->saveField('last_login', date('Y-m-d H:i:s'));

				if ($this->here == $this->Auth->loginRedirect) {
					//$this->Auth->loginRedirect = '/';
				}
				$this->Session->setFlash(sprintf(__d('users', '%s u bent succesvol ingelogd'), $this->Auth->user($this->{$this->modelClass}->displayField)), 'good');
				$this->redirect(array('controller'=>'results', 'action'=> 'me'));
				if (!empty($this->request->data)) {
					$data = $this->request->data[$this->modelClass];
					if (empty($this->request->data[$this->modelClass]['remember_me'])) {
						$this->RememberMe->destroyCookie();
					} else {
						$this->_setCookie();
					}
				}

				if (empty($data[$this->modelClass]['return_to'])) {
					$data[$this->modelClass]['return_to'] = null;
				}

				// Checking for 2.3 but keeping a fallback for older versions
				if (method_exists($this->Auth, 'redirectUrl')) {
					$this->redirect($this->Auth->redirectUrl($data[$this->modelClass]['return_to']));
				} else {
					$this->redirect($this->Auth->redirect($data[$this->modelClass]['return_to']));
				}
			} else {
				$this->Auth->flash(__('Ongeldig e-mail / wachtwoord combinatie. Probeer het nogmaals.'), 'bad');
			}
		}
		if (isset($this->request->params['named']['return_to'])) {
			$this->set('return_to', urldecode($this->request->params['named']['return_to']));
		} else {
			$this->set('return_to', false);
		}
		$allowRegistration = Configure::read('Users.allowRegistration');
		$this->set('allowRegistration', (is_null($allowRegistration) ? true : $allowRegistration));
	}

/**
 * Common logout action
 *
 * @return void
 */
	public function logout() {
		$user = $this->Auth->user();
		$this->Session->destroy();
		$this->RememberMe->destroyCookie();
		$this->Session->setFlash(sprintf(__('%s u bent succesvol uitgelogd'), $user[$this->{$this->modelClass}->displayField]), 'good');
		$this->redirect($this->Auth->logout());
	}

	public function signedup(){
		
	}
	
/**
 * Edit the current logged in user
 *
 * Extend the plugin and implement your custom logic here
 *
 * @return void
 */
	public function edit() {
				try {
			$result = $this->{$this->modelClass}->edit($this->Auth->user('id'), $this->request->data);
			if ($result === true) {
				$this->Session->setFlash(__('Opgeslagen'), 'good');
				$this->redirect($this->referer());
			} else {
				unset($result[$this->modelClass]['password']);
				$this->request->data = $result;
			}
		} catch (OutOfBoundsException $e) {
			$this->Session->setFlash($e->getMessage());
			$this->redirect(array('action' => 'index'));
		}

		if (empty($this->request->data)) {
			$this->request->data = $this->{$this->modelClass}->read(null, $this->Auth->user('id'));
			unset($this->request->data[$this->modelClass]['password']);
		}
	}
}
