<?php
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ResultsController extends AppController {
	
	public $components=array('Calculate','Resultcomp','Generalcomp');
	public $helpers=array('App');
	
	public function me(){
		$oUser=$this->Auth->user();
		$session_id=$this->Session->read('SessionId');
		$results=$this->Result->find('all', array('conditions'=> array('user_id'=> $oUser['id']),'order'=>'id DESC') );
		$check_sessions=$this->Result->find('all', array('conditions'=> array('user_id'=> $oUser['id'], 'session_id'=>$session_id),'order'=>'id DESC') );
		$number_of_segments=count($check_sessions);

		$session_data=array();
		$segments=array();
		
		if($check_sessions){
			for ($i=0; $i < $number_of_segments; $i++) { 
				$session_data[]=unserialize($results[$i]['Result']['session_data']);
				// Segments
				$segments[]=$session_data[0][0]['Page']['segment1'];
				$segment_ids[]=$results[$i]['Result']['id'];
			}
			$this->set('segments',array_combine($segment_ids, $segments));
		}

		$this->set('segments',$segments);
		$this->set('results',$results);
		$this->set('sessionId',$session_id);
	}
		
	public function view($id=null){
		$oUser=$this->Auth->user();	
		if($oUser['is_admin'])
		{
			$result=$this->Result->find('first', array('conditions'=> array('id'=>$id) ) );
			// Retrieve records based on current session id
			$results=$this->Result->find('all', array('conditions'=> array('session_id'=> $result['Result']['session_id']), 'limit'=>3,'order'=>'id ASC') );
		}else{
			$result=$this->Result->find('first', array('conditions'=> array('id'=>$id,'user_id'=> $oUser['id']) ) );
			if(!$result)
			{
				// Someone try to hijack
				$this->redirect('/');
			}
			// Retrieve records based on current session id
			$results=$this->Result->find('all', array('conditions'=> array('session_id'=> $result['Result']['session_id'],'user_id'=> $oUser['id']), 'limit'=>3,'order'=>'id ASC') );
		}	
	
		$number_of_segments=count($results);
		
		$session_data=array();
		$segments=array();
		
		for ($i=0; $i < $number_of_segments; $i++) { 
			$session_data=unserialize($results[$i]['Result']['session_data']);
			// Segments
			$segments[]=$session_data[0]['Page']['segment1'];
			$segment_ids[]=$results[$i]['Result']['id'];
		}

		// Session data from every page		
		$session_data=unserialize($result['Result']['session_data']);
		
		// Segment name
		$segment=$session_data[0]['Page']['segment1'];

		// Hotel own rooms. Integer.
		$own_hotel_rooms=$session_data[0]['Page']['own_rooms'];

		// Hotels info. Array. hotel_short_names, hotel_rooms, hotel_review_scores.
		$hotels_info=$this->Resultcomp->GetHotelsInfo($session_data,$result['Result']['hotel_amount']);
		$hotel_rooms=$hotels_info['hotel_rooms'];
	
		$review_scores = $hotels_info['hotel_review_scores'];
		$review_scores_average_own_hotel=100;
		
		// Calculate average review scores
		for ($l=1; $l <= $result['Result']['hotel_amount']; $l++) { 
			$review_scores_averages[]=$this->Resultcomp->GetAverageReviewScore($session_data,$l,$review_scores_average_own_hotel);
		}
		
		$post_kwaliteitskenmerken=$session_data[3]['dropdown_weight2'];
		
		$waardekenmerk_kamerprijs=array($post_kwaliteitskenmerken[0]);
		unset($post_kwaliteitskenmerken[0]);
		
		$waardekenmerken=$post_kwaliteitskenmerken;
		$rescaledgewicht=array_sum($waardekenmerken);

		$kwaliteitskenmerken=$this->Resultcomp->GetQualities($session_data);
			
		$kwaliteitskenmerken_uw_hotel=array(1=>100,2=>100,3=>100,4=>100,5=>100,6=>100,7=>100,8=>100,9=>100);									
		
		$l=0;
		for($m=1;$m <=$result['Result']['hotel_amount'];$m++){
			$kenmerken_merged[]=$this->Resultcomp->MergeNegativPositiv($m,$session_data);
			$GetCalculatedNegativPositiv2[]=$this->Resultcomp->GetCalculatedNegativPositiv($this->Resultcomp->MergeNegativPositiv($m,$session_data),$kwaliteitskenmerken_uw_hotel,$review_scores_averages[$l]);
			$l++;
		}

		// Get all qualities for each hotel
		$segmenten=$this->Resultcomp->GetQualitiesForHotels($oUser['hotel'],$hotels_info['hotel_short_names'],$kwaliteitskenmerken,$GetCalculatedNegativPositiv2,1);
		$prijsniveau_hotels=$this->Resultcomp->GetPriceLevels($session_data,$result['Result']['hotel_amount']);
		
		$kamerprijs_uw_hotel=array($session_data[1]['Page']['prijsniveau_own_hotel1'],$session_data[1]['Page']['prijsniveau_own_hotel2'],$session_data[1]['Page']['prijsniveau_own_hotel3']);
		
		// Check for each hotel is the value is set 
		 $prijsniveau_h1= (isset($prijsniveau_hotels[0][0])) ? $prijsniveau_hotels[0][0] : '';
		 $prijsniveau_h2= (isset($prijsniveau_hotels[0][1])) ? $prijsniveau_hotels[0][1] : '';
		 $prijsniveau_h3= (isset($prijsniveau_hotels[0][2])) ? $prijsniveau_hotels[0][2] : '';
		 $prijsniveau_h4= (isset($prijsniveau_hotels[0][3])) ? $prijsniveau_hotels[0][3] : '';
		 $prijsniveau_h5= (isset($prijsniveau_hotels[0][4])) ? $prijsniveau_hotels[0][4] : ''; 

		$kamerprijs_gemiddeld=array($prijsniveau_h1,$prijsniveau_h2,$prijsniveau_h3,$prijsniveau_h4,$prijsniveau_h5);
		
		// Get average room prices. Array key => value. 0 to 4.
		$average_room_prices= $this->Calculate->GetAverageRoomPrice($this->Resultcomp->array_remove_empty($kamerprijs_gemiddeld),$kamerprijs_uw_hotel[0]);		
		
		$d122= ( ($kamerprijs_uw_hotel[0]/$kamerprijs_uw_hotel[0])*100 );
		
		// Rescaled Weights. Array key => value. 0 to 9.
		$RescaledWeights=$this->Calculate->RescaledWeights($waardekenmerken,$rescaledgewicht);
		
		// Axis
		$CoordsxAxis=$this->Calculate->CoordsxAxis($oUser['hotel'],$segmenten,$RescaledWeights,$hotels_info['hotel_short_names']);
		
		$CoordsyAxis=$this->Calculate->CoordsyAxis($average_room_prices);
		
		// Load text from database
		$blocks=array( $this->getTextBlocks('ResultsBlock1,ResultsBlock2') );
		$this->set('text_blocks',$blocks[0]);
	
		// Parse all data to view		
		$this->set('average_room_prices',$average_room_prices);		
		$this->set('kamerprijs_gemiddeld',	$kamerprijs_gemiddeld);		
		$this->set('CoordsxAxis',$CoordsxAxis);
		$this->set('CoordsyAxis',$CoordsyAxis);
		$this->set('RescaledWeights',$RescaledWeights);
		$this->set('hotel_rooms',$hotel_rooms);
		$this->set('hotel_name',$oUser['username']);
		$this->set('hotel_full_name',$oUser['hotel']);
		$this->set('hotels',$hotels_info['hotel_short_names']);
		$this->set('waardekenmerk_kamerprijs',$waardekenmerk_kamerprijs);
		$this->set('d122',$d122);
		$this->set('kamerprijs_uw_hotel',$kamerprijs_uw_hotel);
		$this->set('review_scores',$review_scores);
		$this->set('kwaliteitskenmerken_uw_hotel',$kwaliteitskenmerken_uw_hotel);
		$this->set('kwaliteitskenmerken',$kwaliteitskenmerken);
		$this->set('segmenten',$segmenten);
		$this->set('own_hotel_rooms',$own_hotel_rooms);
		$this->set('segment',$segment);
		$this->set('segments',array_combine($segment_ids, $segments));
		$this->set('result',$result);
		$this->set('review_scores_averages',$review_scores_averages);
		$this->set('hotel_amount',$result['Result']['hotel_amount']);
		$this->set('id',$id);
	}

	public function dynamicchart($id=null){
		$oUser=$this->Auth->user();	
		$this->autoRender = false;

		$result=$this->Result->find('first', array('conditions'=> array('id'=>$id) ) );


		// Session data from every page		
		$session_data=unserialize($result['Result']['session_data']);
		
		// Segment name
		$segment=$session_data[0]['Page']['segment1'];

		// Hotel own rooms. Integer.
		$own_hotel_rooms=$session_data[0]['Page']['own_rooms'];

		// Hotels info. Array. hotel_short_names, hotel_rooms, hotel_review_scores.
		$hotels_info=$this->Resultcomp->GetHotelsInfo($session_data,$result['Result']['hotel_amount']);
		$hotel_rooms=$hotels_info['hotel_rooms'];
	
		$review_scores = $hotels_info['hotel_review_scores'];
		$review_scores_average_own_hotel=100;
		
		// Calculate average review scores
		for ($l=1; $l <= $result['Result']['hotel_amount']; $l++) { 
			$review_scores_averages[]=$this->Resultcomp->GetAverageReviewScore($session_data,$l,$review_scores_average_own_hotel);
		}
		
		$post_kwaliteitskenmerken=$session_data[3]['dropdown_weight2'];
		
		$waardekenmerk_kamerprijs=array($post_kwaliteitskenmerken[0]);
		unset($post_kwaliteitskenmerken[0]);
		
		$waardekenmerken=$post_kwaliteitskenmerken;
		$rescaledgewicht=array_sum($waardekenmerken);

		$kwaliteitskenmerken=$this->Resultcomp->GetQualities($session_data);
			
		$kwaliteitskenmerken_uw_hotel=array(1=>100,2=>100,3=>100,4=>100,5=>100,6=>100,7=>100,8=>100,9=>100);									
		
		$l=0;
		for($m=1;$m <=$result['Result']['hotel_amount'];$m++){
			$kenmerken_merged[]=$this->Resultcomp->MergeNegativPositiv($m,$session_data);
			$GetCalculatedNegativPositiv2[]=$this->Resultcomp->GetCalculatedNegativPositiv($this->Resultcomp->MergeNegativPositiv($m,$session_data),$kwaliteitskenmerken_uw_hotel,$review_scores_averages[$l]);
			$l++;
		}

		// Get all qualities for each hotel
		$segmenten=$this->Resultcomp->GetQualitiesForHotels($oUser['hotel'],$hotels_info['hotel_short_names'],$kwaliteitskenmerken,$GetCalculatedNegativPositiv2,1);
		$prijsniveau_hotels=$this->Resultcomp->GetPriceLevels($session_data,$result['Result']['hotel_amount']);
		
		$kamerprijs_uw_hotel=array($session_data[1]['Page']['prijsniveau_own_hotel1'],$session_data[1]['Page']['prijsniveau_own_hotel2'],$session_data[1]['Page']['prijsniveau_own_hotel3']);
		
		extract($_GET);
		// Check for each hotel is the value is set 
		 $prijsniveau_h1= (isset($hotel0_kamerprijs)) ? $hotel0_kamerprijs : $prijsniveau_hotels[0][0];
		 $prijsniveau_h2= (isset($hotel1_kamerprijs)) ? $hotel1_kamerprijs : $prijsniveau_hotels[0][1];
		 $prijsniveau_h3= (isset($hotel2_kamerprijs)) ? $hotel2_kamerprijs : $prijsniveau_hotels[0][2];
		 $prijsniveau_h4= (isset($hotel3_kamerprijs)) ? $hotel3_kamerprijs : $prijsniveau_hotels[0][3];
		 $prijsniveau_h5= (isset($hotel4_kamerprijs)) ? $hotel4_kamerprijs : $prijsniveau_hotels[0][4]; 

		$kamerprijs_gemiddeld=array($prijsniveau_h1,$prijsniveau_h2,$prijsniveau_h3,$prijsniveau_h4,$prijsniveau_h5);
		
		// Get average room prices. Array key => value. 0 to 4.
		$average_room_prices= $this->Calculate->GetAverageRoomPrice($this->Resultcomp->array_remove_empty($kamerprijs_gemiddeld),$hotelownhotel_kamerprijs);		
		
		$d122= ( ($hotelownhotel_kamerprijs/$hotelownhotel_kamerprijs)*100 );
		
		// Rescaled Weights. Array key => value. 0 to 9.
		$RescaledWeights=$this->Calculate->RescaledWeights($waardekenmerken,$rescaledgewicht);
		
		// Axis
		$CoordsxAxis=$this->Calculate->CoordsxAxis($oUser['hotel'],$segmenten,$RescaledWeights,$hotels_info['hotel_short_names']);
		
		$CoordsyAxis=$this->Calculate->CoordsyAxis($average_room_prices);
		/*echo array(
					json_encode($CoordsyAxis),
					json_encode($CoordsxAxis)
					);*/
		echo json_encode($CoordsyAxis);
	}

	public function admin_index(){
		$this->paginate = array(
		    'joins' => array(
			array(
		            'table' => 'users',
		            'alias' => 'UserJoin',
		            'type' => 'INNER',
		            'conditions' => array(
		                'UserJoin.id = Result.user_id'
		            )
		        )
		    ),
		    'order'=> 'Result.id DESC',
		    'fields' => array('UserJoin.*','Result.*')
		);
		
		$results=$this->paginate('Result');
		$this->set('results',$results);
	}
}