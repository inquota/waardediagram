<?php
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SegmentsController extends AppController {
	
	public $components=array('Generalcomp', 'Resultcomp');
	public $helpers=array('App');
	
	/**
	 * choose
	 */
	public function choose(){
		$oUser=$this->Auth->user();
		
		if(!$this->Session->read('SessionId')){
			// Generate Session ID
			$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		    $result = '';
		    for ($i = 0; $i < 33; $i++)
			$result .= $characters[mt_rand(0, 61)];		
			$this->Session->write('SessionId', strtoupper($result));
		}
		
		if(!empty($this->data))
		{
			$pageData= array_filter($this->data);
			$hotel_amount=array($this->data['Page']['abrv1'],$this->data['Page']['abrv2'],$this->data['Page']['abrv3'],$this->data['Page']['abrv4'],$this->data['Page']['abrv5']);
			$hotel_amount=count(array_filter($hotel_amount));
			
			$this->Session->write('PageData',$pageData);
			$this->Session->write('HotelAmount',$hotel_amount);
			$this->redirect( array('controller'=>'segments', 'action'=> 'step/1/') );
		}
		
		// Check if Session PageData isset so we can use it in our form
		if($this->Session->check('PageData')){
			$this->data = $this->Session->read('PageData'); 
		}
		
		$blocks=array( $this->getTextBlocks('ChooseBlock1,ChooseBlock2') );
		$this->set('blocks',$blocks[0]);
		$this->set('hotel_name',$oUser['username']);
	}
	
	/**
	 * step
	 */
	public function step( $step=null ){
		$this->loadModel('Quality');
		$oUser=$this->Auth->user();
		$this->set('hotel_name',$oUser['username']);
		$SessionPageData=$this->Session->read('PageData');
		$HotelAmount=$this->Session->read('HotelAmount');
		$this->set('HotelAmount',$HotelAmount);
	
		for($percent_negative=5;$percent_negative<125;$percent_negative+=5)
		{
			$aPercent_negative[]= '-'.$percent_negative;
		}
	    
		for($percent_positive=5;$percent_positive<125;$percent_positive+=5)
		{
			$aPercent_positive[]= '+'.$percent_positive;
		}
		
		$this->set('aPercent_negative',$aPercent_negative);
		$this->set('aPercent_positive',$aPercent_positive);
		$this->set('PageData', $this->Session->read('PageData') );
		
		switch ($step) {
			case 1:
				// Load the right view
				$this->view = 'prijskenmerken';
				
				// Check if the basic data is set
				if( $this->Session->read('PageData') == false ){
					$this->redirect('/');
				}
				
				// If we set the data, put it in a session
				if(!empty($this->data))
				{
					$this->Session->write('Session1',$this->data);
					$this->redirect( array('controller'=>'segments', 'action'=> 'step/2/') );
				}
				
				// Load text from database
				$blocks=array( $this->getTextBlocks('Step1Block1') );
				$parameters=array('segment_titel'=>$SessionPageData['Page']['segment1']);
				$text_blocks=$this->Generalcomp->CompileText($blocks,$parameters);
				$this->set('text_blocks',$text_blocks);
				
				// Check if Session PageData isset so we can use it in our form
				if($this->Session->check('Session1')){
					$this->data = $this->Session->read('Session1'); 
				}
						
				break;
				
			case 2:
				// Load the right view
				$this->view = 'kwaliteitskenmerken';
				
				// Check if the basic data is set and SessionPrijskenmerken
				if( $this->Session->read('PageData') == false && $this->Session->read('Session1') == false ){
					$this->redirect('/');
				}
				
				// If we set the data, put it in a session
				if(!empty($this->data))
				{
					$this->Session->write('Session2',$this->data);					
					$this->redirect( array('controller'=>'segments', 'action'=> 'step/3/') );
				}
				
				$kwaliteitskenmerken=$this->Quality->find('all', array('order'=>'sequence ASC') );
				foreach($kwaliteitskenmerken as $v){
					$kwaliteitskenmerken_final[]= $v['Quality']['title'];
				}
				
				// Load text from database
				$blocks=array( $this->getTextBlocks('Step2Block1,Step2Block2') );
				$parameters=array('segment_titel'=>$SessionPageData['Page']['segment1']);
				$text_blocks=$this->Generalcomp->CompileText($blocks,$parameters);
				
				$this->set('text_blocks',$text_blocks);
				$this->set('kwaliteitskenmerken',$kwaliteitskenmerken_final);				
				$this->set('kenmerken_merged', array());
				$this->set('session2',array());
				// Check if Session PageData isset so we can use it in our form
				if($this->Session->check('PageData') && $this->Session->check('Session2')){
					$this->data = $this->Session->read('Session2'); 
					$PageData=$this->Session->read('PageData');
					$session2=$this->Session->read('Session2');
					$hotels=array($PageData['Page']['name_short1'],$PageData['Page']['name_short2'],$PageData['Page']['name_short3'],$PageData['Page']['name_short4'],$PageData['Page']['name_short5']);
					
					$review_scores = array($session2['Page']['reviewscores_hotel1'],$session2['Page']['reviewscores_hotel2'],$session2['Page']['reviewscores_hotel3'],$session2['Page']['reviewscores_hotel4'],$session2['Page']['reviewscores_hotel5']);
					$review_scores_average_own_hotel=100;
					
					// Calculate average review scores
					for ($l=1; $l <= 5; $l++) { 
						$review_scores_averages[]=$this->Resultcomp->GetAverageReviewScore($session2,$l,$review_scores_average_own_hotel,1);
					}
		
					$kwaliteitskenmerken_uw_hotel=array(1=>100,2=>100,3=>100,4=>100,5=>100,6=>100,7=>100,8=>100,9=>100);
					
					$l=0;
					for($m=1;$m <=$HotelAmount;$m++){
						$kenmerken_merged[]=$this->Resultcomp->MergeNegativPositiv($m,$session2,1);
						$GetCalculatedNegativPositiv2[]=$this->Resultcomp->GetCalculatedNegativPositiv($this->Resultcomp->MergeNegativPositiv($m,$session2,1),$kwaliteitskenmerken_uw_hotel,$review_scores_averages[$l]);
						$l++;
					}

					$segmenten=$this->Resultcomp->GetQualitiesForHotels($oUser['hotel'],$hotels,$kwaliteitskenmerken,$GetCalculatedNegativPositiv2,0,0);
					$this->set('kenmerken_merged', $kenmerken_merged);
					$this->set('session2',$session2);
				}
	
				break;
				
			case 3:
				// Load the right view
				$this->view = 'wegingsfactoren';
				
				// Check if the basic data is set and SessionPrijskenmerken
				if( $this->Session->read('PageData') == false && $this->Session->read('Session1') == false && $this->Session->read('Session2') == false ){
					$this->redirect('/');
				}
		
				// If we set the data, put it in a session
				if(!empty($this->data))
				{
					$this->Session->write('Session3',$this->data);
					
					// Save all data to model
					$this->loadModel('Result');
					
					$PageDataFinal=array(0=>$this->Session->read('PageData'),1=>$this->Session->read('Session1'),2=>$PageDataFinal[2]=$this->Session->read('Session2'),3=>$this->Session->read('Session3'));
		
					$this->request->data['Result']['user_id'] = $oUser['id'];
					$this->request->data['Result']['session_data'] = serialize($PageDataFinal);
					$this->request->data['Result']['hotel_amount'] = $this->Session->read('HotelAmount');
					$this->request->data['Result']['created_dt'] = date('c');
					$this->request->data['Result']['session_id'] = $this->Session->read('SessionId');
					$this->Result->create();
					$this->Result->save($this->request->data);
					
					$this->redirect( array('controller'=> 'results','action'=> 'me') );
				}
				
				$kwaliteitskenmerken=$this->Session->read('Session2');
				$kwaliteitskenmerken_aantal=9;
				for($z=1;$z <= $kwaliteitskenmerken_aantal;$z++){
					$kwaliteitskenmerken_labels[]= $kwaliteitskenmerken['Page']['kwaliteitskenmerk'.$z];
				}
				
				// Load text from database
				$blocks=array( $this->getTextBlocks('Step3Block1,Step3Block2') );
				$parameters=array('segment_titel'=>$SessionPageData['Page']['segment1']);
				$text_blocks=$this->Generalcomp->CompileText($blocks,$parameters);
				
				$this->set('text_blocks',$text_blocks);
				$this->set('kwaliteitskenmerken',$kwaliteitskenmerken_labels);
				
				// Check if Session Session3 isset so we can use it in our form
				$this->set('session3',array());
				if($this->Session->check('Session3')){
					$this->data = $this->Session->read('Session3'); 
					$session3=$this->Session->read('Session3');
				
					$this->set('session3',$session3);
				}
				
				break;
						
			default:
				
				break;
		}
	}
		
	/**
	 * restart
	 */
	public function restart(){
		// Unset current sessions and redirect to choose
		$this->Session->delete('PageData');
		$this->Session->delete('Session1');
		$this->Session->delete('Session2');
		$this->Session->delete('Session3');
		$this->redirect( array('action'=> 'choose') );
	}
}