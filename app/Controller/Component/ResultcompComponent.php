<?php
/**
 * ResultcompComponent
 * @author 			Jarah de Jong
 * Description: 	This component use small kinds of logic for the ResultsController
 * Date:			23-08-2014
 */
class ResultcompComponent extends Component {
	/**
	 * GetQualities
	 * @param Array $hotels
	 * @param Array $qualities
	 * @param Array $GetCalculatedNegativPositiv
	 * @return Array $aQualities
	 */	
	public function GetQualitiesForHotels($hotel_logged_in,$hotels,$qualities,$GetCalculatedNegativPositiv,$use_in_results=1)
	{
		if($use_in_results==0) :
			$own_hotel= array(
						$hotel_logged_in=> array(
										$qualities[0]['Quality']['title']=>100,
										$qualities[1]['Quality']['title']=>100,
										$qualities[2]['Quality']['title']=>100,
										$qualities[3]['Quality']['title']=>100,
										$qualities[4]['Quality']['title']=>100,
										$qualities[5]['Quality']['title']=>100,
										$qualities[6]['Quality']['title']=>100,
										$qualities[7]['Quality']['title']=>100,
										$qualities[8]['Quality']['title']=>100			
										)
					);
		elseif($use_in_results==1) : 
			$own_hotel= array(
				$hotel_logged_in=> array(
								$qualities[0]=>100,
								$qualities[1]=>100,
								$qualities[2]=>100,
								$qualities[3]=>100,
								$qualities[4]=>100,
								$qualities[5]=>100,
								$qualities[6]=>100,
								$qualities[7]=>100,
								$qualities[8]=>100,
								$qualities[9]=>100				
								)
			);
		endif;
	
	$h=0;
	for($i=1;$i <= count($hotels); $i++) {
		$aQualities[]=$this->GetHotelQualities($hotels,$qualities,$GetCalculatedNegativPositiv,$h,$use_in_results);
		$aQualities=array_merge($own_hotel,$aQualities);
		$h++;
	}
		return $aQualities;
	}

	/**
	 * GetHotelQualities
	 * @param Array $GetCalculatedNegativPositiv
	 * @param Integer $key
	 * @param Array
	 */
	public function GetHotelQualities($hotels,$qualities,$GetCalculatedNegativPositiv,$key,$use_in_results=1){
		if($use_in_results==0) :
				return  array(
						$hotels[$key]=> array(
											'Review Score'=>$GetCalculatedNegativPositiv[$key][0],
											$qualities[0]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][0],
											$qualities[1]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][1],
											$qualities[2]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][2],
											$qualities[3]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][3],
											$qualities[4]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][4],
											$qualities[5]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][5],
											$qualities[6]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][6],
											$qualities[7]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][7],
											$qualities[8]['Quality']['title']=>$GetCalculatedNegativPositiv[$key][8]	
						)
					);
		elseif($use_in_results==1) :
				return array(
						$hotels[$key]=> array(
											$qualities[0]=>$GetCalculatedNegativPositiv[$key][0],
											$qualities[1]=>$GetCalculatedNegativPositiv[$key][1],
											$qualities[2]=>$GetCalculatedNegativPositiv[$key][2],
											$qualities[3]=>$GetCalculatedNegativPositiv[$key][3],
											$qualities[4]=>$GetCalculatedNegativPositiv[$key][4],
											$qualities[5]=>$GetCalculatedNegativPositiv[$key][5],
											$qualities[6]=>$GetCalculatedNegativPositiv[$key][6],
											$qualities[7]=>$GetCalculatedNegativPositiv[$key][7],
											$qualities[8]=>$GetCalculatedNegativPositiv[$key][8],
											$qualities[9]=>$GetCalculatedNegativPositiv[$key][9]			
						)
					);
		endif;
	}

	/**
	 * GetQualities
	 * Get qualities array; like array('Quality1','Quality2','Quality3');
	 * @param Array $session_data
	 * @param Integer $qualities_amount, default=9
	 * @return Array $qualities
	 */
	public function GetQualities($session_data,$qualities_amount=9){
			$qualities_temp=array('Review score');
			$qualities=array();
			for($u=1;$u <= $qualities_amount;$u++) {
				$qualities[]= $session_data[2]['Page']['kwaliteitskenmerk'.$u];
			}		
			$qualities=array_merge($qualities_temp, $qualities);
			return $qualities;
		}
	
	/**
	 * GetPriceLevels
	 * @param Array $session_data
	 * @param Integer $hotel_amount, default = 5
	 * @return Array $prijsniveau_hotels
	 */
	public function GetPriceLevels($session_data,$hotel_amount=5){
		// Loop through prijsniveau_hotel
		$prijsniveau_hotels=array();		
		for ($i=1; $i <= $hotel_amount; $i++) { 
			$prijsniveau_hotels_gemiddeld[]= $session_data[1]['Page']['prijsniveau_hotel_gemiddeld'.$i];
			$prijsniveau_hotels_hoogste[]=$session_data[1]['Page']['prijsniveau_hotel_hoogste'.$i];
			$prijsniveau_hotels_laagste[]=$session_data[1]['Page']['prijsniveau_hotel_laagste'.$i];
		}	
		return array($prijsniveau_hotels_gemiddeld,$prijsniveau_hotels_hoogste,$prijsniveau_hotels_laagste);
	}
	
	/**
	 * PrettifySegments
	 * @param Integer $number_of_segments
	 * @param Array $segments
	 * @return Array $array_keys;
	 */
	public function PrettifySegments($number_of_segments,$segments){
		$array_keys=array();
		for ($j=0; $j < $number_of_segments; $j++) { 
			$array_keys[]= $this->Slugify( $segments[$j] );
		}
		return array_combine($array_keys, $segments);
	}
	
	/**
	 * Slugify
	 * @param String $text
	 * @param String $text
	 * @return String $text
	 */
	public function Slugify($text){ 
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	
	  // trim
	  $text = trim($text, '-');
	
	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	
	  // lowercase
	  $text = strtolower($text);
	
	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);
	
	  if (empty($text))
	  {
	    return 'n-a';
	  }
	
	  return $text;
	}

	/**
	 * array_remove_empty
	 * @param Array $arr
	 * @return Array $narr
	 */
	public function array_remove_empty($arr){
        $narr = array();
        while(list($key, $val) = each($arr)){
            if (is_array($val)){
                $val = array_remove_empty($val);
                // does the result array contain anything?
                if (count($val)!=0){
                    // yes :-)
                    $narr[$key] = $val;
                }
            }
            else {
                if (trim($val) != ""){
                    $narr[$key] = $val;
                }
            }
        }
        unset($arr);
        return $narr;
    }
	
	/**
	 * MergeNegativPositiv
	 * @param Integer $hotel_nr
	 * @param Array $session
	 * @return Array $kenmerken_merged
	 */
	public function MergeNegativPositiv($hotel_nr,$session,$edit_mode=0){
		if($edit_mode==1){
			$kenmerken_merged=$this->array_remove_empty($session['kenmerken_neg_hotel'.$hotel_nr])+$this->array_remove_empty($session['kenmerken_pos_hotel'.$hotel_nr]);
		}elseif($edit_mode==0){
			$kenmerken_merged=$this->array_remove_empty($session[2]['kenmerken_neg_hotel'.$hotel_nr])+$this->array_remove_empty($session[2]['kenmerken_pos_hotel'.$hotel_nr]);	
		}	
		ksort($kenmerken_merged);
		return($kenmerken_merged);
	}
	
	/**
	 * GetCalculatedNegativPositiv
	 * @param Array $kenmerken_merged
	 * @param Array $kwaliteitskenmerken_uw_hotel
	 * @param Float $review_scores_averages
	 * @param Integer $segments, default 9 
	 * @return Array $r
	 */
	public function GetCalculatedNegativPositiv($kenmerken_merged,$kwaliteitskenmerken_uw_hotel,$review_scores_average,$segments=9){
		$r=array();
		$r[]=(integer) $review_scores_average;
		for ($j=1; $j <= $segments; $j++) {			
			$add_or_substr=substr($kenmerken_merged[$j-1], 0,1); 
			$number=substr($kenmerken_merged[$j-1], 1); 			
			if($add_or_substr=='+')
			{
				$r[]= $kwaliteitskenmerken_uw_hotel[1]+$number;
			}elseif($add_or_substr=='-'){
				$r[]= $kwaliteitskenmerken_uw_hotel[1]-$number;
			}elseif($kenmerken_merged[$j-1] == 0){
				$r[]=100;
			}
		}
		return $r;
	}
	
	/**
	 * GetAverageReviewScore
	 * @param Array $session_data
	 * @param Integer $hotel_nr
	 * @param Integer $review_scores_average_own_hotel
	 * @return Float
	 */
	public function GetAverageReviewScore($session_data,$hotel_nr,$review_scores_average_own_hotel,$edit_mode=0){
		if($edit_mode==1)
		{
			return  number_format( ($session_data['Page']['reviewscores_hotel'.$hotel_nr]/$session_data['Page']['review_score_uw_hotel']) * $review_scores_average_own_hotel,0);
		}elseif($edit_mode==0){
			return  number_format( ($session_data[2]['Page']['reviewscores_hotel'.$hotel_nr]/$session_data[2]['Page']['review_score_uw_hotel']) * $review_scores_average_own_hotel,0);	
		}		
	}
	
	/**
	 * GetHotelsInfo
	 * @param Array $session_data
	 * @param Integer $hotels_amount, default=5
	 * @return Array $hotel_short_names
	 */
	public function GetHotelsInfo($session_data,$hotels_amount=5){
		$hotel_short_names	=	array();
		$hotel_rooms		=	array();
		for($i=1;$i <= $hotels_amount;$i++){
			$hotel_short_names[]	=	$session_data[0]['Page']['name_short'.$i];
			$hotel_rooms[]			=	$session_data[0]['Page']['rooms'.$i];
			$hotel_review_scores[] 	=	$session_data[2]['Page']['reviewscores_hotel'.$i];
		}
		return array('hotel_short_names'=>$hotel_short_names,'hotel_rooms'=>$hotel_rooms,'hotel_review_scores'=>$hotel_review_scores);
	}
}