<?php
/**
 * CalculateComponent
 * @author 			Jarah de Jong
 * Description: 	This component use small kinds of logic to calculate
 * Date:			31-07-2014
 */
class CalculateComponent extends Component {
	/**
	 * GetAverageRoomPrice
	 * @param Array $kamerprijs_gemiddeld
	 * @param Decimal $kamerprijs_uw_hotel
	 * @return Array $r
	 */	
	public function GetAverageRoomPrice($kamerprijs_gemiddeld,$kamerprijs_uw_hotel)
	{
		$r=array();
		foreach($kamerprijs_gemiddeld as $k=>$gemiddeld)
		{
			$r[]= number_format( ($gemiddeld/$kamerprijs_uw_hotel) * 100, 0 );
		}
	return $r;
	}
	/**
	 * RescaledWeights
	 * @param Array $waardekenmerken
	 * @param Decimal $rescaledgewicht
	 * @return Array $r
	 */
	public function RescaledWeights($waardekenmerken,$rescaledgewicht)
	{
		$r=array();
		foreach($waardekenmerken as $waardekenmerk)
		{
			$percentage=($waardekenmerk / $rescaledgewicht)*100;
			$r[]= number_format($percentage,0);
		}	
		return $r;
	}
	/**
	 * CoordsyAxis
	 * @param Array $average_room_prices
	 * @return Array $r
	 */
	public function CoordsyAxis($average_room_prices)
	{
		$r=array();
		foreach($average_room_prices as $k=>$v)
		{
			$d52=$v;
			$c122=100;
			$g122=$c122*$d52;
			
			$g122=($g122/100);
			$g134= ($g122/10)-10;
			$r[]= $g134;
		}
		return $r;
	}
	/**
	 * CoordsxAxisSingle
	 * @param Array $segmenten
	 * @param Array $rescaledweights
	 * @return Array $r
	 */
	public function CoordsxAxisSingle($segmenten, $rescaledweights)
	{
		$r=array();
		$i=0;
		foreach($segmenten as $k=>$v)
		{
			$r[]= number_format((($rescaledweights[$i]/100) * $v),0);
			$i++;
		}
		return $r;
	}
	/**
	 * CoordsxAxis
	 * @param String $hotel_full_name
	 * @param Array $segementen
	 * @param Array $RescaledWeights
	 * @param Array $hotel_short_names
	 * @return Array $r
	 */
	public function CoordsxAxis($hotel_full_name,$segmenten,$RescaledWeights,$hotel_short_names){
		$r=array();
		unset($segmenten[$hotel_full_name]);
		foreach($segmenten as $k=>$v)
		{
			$hotel_name=$hotel_short_names[$k];
			$CoordsxAxis= $this->CoordsxAxisSingle($segmenten[$k][$hotel_name],$RescaledWeights);
			$r[]= (array_sum($CoordsxAxis)/10)-10;
		}
		return $r;
	}
	/**
	 * GenerateSeriesData
	 * @param Decimal $val1
	 * @param Decimal $val2
	 * @param Decimal $val3
	 * @return String $hotel
	 */
	public function GenerateSeriesData($val1,$val2,$val3){
		$hotel=array($val1,$val2,$val3);
		$hotel=implode(',',$hotel);
		return $hotel;
	}
	/**
	 * CoordsNetto
	 * @param Array $CoordsxAxis
	 * @param Array $CoordsyAxis
	 * @return Array $r
	 */
	public function CoordsNetto($CoordsxAxis,$CoordsyAxis)
	{
		$r=array();
		foreach($CoordsxAxis as $k=>$v)
		{
			$r[]= ($v - $CoordsyAxis[$k]);
		}
		return $r;
	}
	/**
	 * GetBeterOfSlechter
	 * @param Decimal $input
	 * @return String beter or slechter
	 */
	public function GetBeterOfSlechter($input){
		if( $input > 0 )
		{
			return __("beter");
		}else{
			return __("slechter");
		}
	}
}