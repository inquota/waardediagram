<?php
/**
 * GeneralcompComponent
 * @author 			Jarah de Jong
 * Description: 	This component use small kinds of logic for the general things
 */
class GeneralcompComponent extends Component {
	/**
	 * ReplacePageVars
	 * @param String $page_vars
	 * @param Array $parameters
	 * @param String $content
	 * @return String $content
	 */	
	public function ReplaceVars($page_vars,$parameters,$content)
	{
		$vars = explode(',',$page_vars);
		foreach($vars as $k=>$v) :
			$needle='['.$v.']';
			
			try
				{
					if($parameters[$v]==NULL){
						throw new Exception('<p class="bg-warning">'.__("Er wordt een variabele gebruikt dat niet bekend is. Neem contact op met de ontwikkelaar van Marktpositie Analyse tool").'</p>');
					}else{
						$target=$parameters[$v];					
					}
				}
			   catch(Exception $e)
			   {
			    	echo $e->getMessage();
				}
			
			$content=str_replace($needle, $target, $content);
		endforeach;
	
		return $content;
	}
	
	/**
	 * CompileText
	 * @param Array $blocks
	 * @param Array $parameters
	 * @return Array $text_blocks
	 */
	public function CompileText($blocks,$parameters){
		$blocks=$blocks[0];
		$text_blocks=array();
		foreach ($blocks as $key => $value) {
			if(!empty($blocks[$key]['Text']['page_vars'])){
				$text_blocks[]=$this->ReplaceVars($blocks[$key]['Text']['page_vars'],$parameters,$blocks[$key]['Text']['tekst']);
			}else{
				$text_blocks[]=$blocks[$key]['Text']['tekst'];
			}
		}		
		return $text_blocks;
	}
}