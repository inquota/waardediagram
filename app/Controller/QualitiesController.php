<?php
App::uses('AppController', 'Controller');

/**
 * Qualities controller
 *
 * With this controller the admin can manage qualities (kwaliteitskenmerken)
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class QualitiesController extends AppController {

	var $name = 'Qualities';
	
	public function admin_index(){
		$this->Quality->recursive = 0;
		$this->set('qualities', $this->paginate());
	}

	public function admin_add() {
		if (!empty($this->data)) {
			$this->Quality->create();
			if ($this->Quality->save($this->data)) {
				$this->Session->setFlash(__('Kwaliteitskenmerk toegevoegd.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Kwaliteitskenmerk niet toegevoegd, probeer het opnieuw.', true));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Ongeldige id', true));
			$this->redirect(array('action' => 'index'));
		}
		
		if (!empty($this->data)) {			
			$error=0;
			if($error==0){
				if ($this->Quality->validates()) {
					$this->request->data['Kwaliteitskenmerk']['title'] = utf8_encode($this->request->data['Kwaliteitskenmerk']['title']);
					$this->Quality->id = $id;
					$this->Quality->save($this->data);
					$this->Session->setFlash(__('Kwaliteitskenmerk bijgewerkt.', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Kwaliteitskenmerk is niet bijgewerkt. probeer het opnieuw.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Quality->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Ongeldige id', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Quality->delete($id)) {
			$this->Session->setFlash(__('Kwaliteitskenmerk verwijderd', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Kwaliteitskenmerk niet verwijderd', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>