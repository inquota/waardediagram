<?php
/**
 * Application UserController
 *
 *
 * @package       app.AppUserController
 * @since         
 */

App::uses('AppController', 'Controller');

/**
 * AppUserController
 *
 * Extending the Users Plugin.
 *
 * @package		app.AppUserController
 */
class TestController extends AppController
{
	
	/**
	 * beforeFilter
	 *
	 * Setup all the Auth settings here
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		$this->Auth->allow('test_public');
	}
	
	/**
	 * index
	 *
	 * Index action, required login
	 */
	public function index()
	{
		$this->autoRender = false;
		
		echo "You are logged in";
	}
	
	/**
	 * test
	 *
	 * Test action, required login
	 */
	public function test()
	{
		$this->autoRender = false;
		
		echo "You are logged in";
	}
	
	/**
	 * test_public
	 *
	 * Test_public, visible without login
	 */
	public function test_public()
	{
		$this->autoRender = false;
		
		echo "Everyone can see this";
	}
	
	/**
	 * admin_index
	 *
	 * admin_index, required login (admin)
	 */
	public function admin_index()
	{
		$this->autoRender = false;
		
		echo $this->Auth->user('is_admin');
		echo "<br />";
		echo "Admin index";
	}
	
	/**
	 * admin_test
	 *
	 * admin_test, required login (admin)
	 */
	public function admin_test()
	{
		$this->autoRender = false;
		
		echo "Admin test";
	}
}
