<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	
/**
 * Beforefilter
 *
 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		// Allow all static pages
		$this->Auth->allow();
	}
	

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;
		
		$blocks=array( $this->getTextBlocks('Home,Home2,HowItWorks') );
		$this->set('blocks',$blocks[0]);

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}		
	}
	
	public function info(){
		debug( $this->request->data );
	}
	
	public function howitworks(){
		$blocks=array( $this->getTextBlocks('HowItWorks') );
		$this->set('blocks',$blocks[0]);
	}
	
	public function language($lang=null){
		$this->autoRender = false;
		$this->layout = false;
		
		switch ($lang) {
			case 'eng':
					$this->Session->write('language_manual',1);
					$this->Session->write('Config.language', 'eng');
				$this->redirect($this->referer());
				break;
				
			case 'nld':
					$this->Session->write('language_manual',1);
					$this->Session->write('Config.language', 'nld');
					$this->redirect($this->referer());
				break;
			
			default:
				// Language does nt exists
				$this->redirect('/');
				break;
		}
	}
	
	public function admin_dashboard(){
		
	}
}