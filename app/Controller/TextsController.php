<?php
App::uses('AppController', 'Controller');

/**
 * Texts controller
 *
 * With this controller the admin can manage text blocks in the website
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TextsController extends AppController {

	var $name = 'Texts';
	
	public function admin_index(){
		$this->Text->recursive = 0;
		$this->set('users', $this->paginate());
	}

	public function admin_add() {
		if (!empty($this->data)) {
			$this->Text->create();
			if ($this->Text->save($this->data)) {
				$this->Session->setFlash(__('Tekst blok toegevoegd.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Tekst blok niet toegevoegd, probeer het opnieuw.', true));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Ongeldige id', true));
			$this->redirect(array('action' => 'index'));
		}
		
		if (!empty($this->data)) {
			
			$error=0;
			if($error==0){
				if ($this->Text->validates()) {
					$this->request->data['Text']['tekst'] = utf8_encode($this->request->data['Text']['tekst']);
					$this->Text->id = $id;
					$this->Text->save($this->data);
					$this->Session->setFlash(__('Tekst blok bijgewerkt.', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Tekst blok is niet bijgewerkt. probeer het opnieuw.', true));
				}
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Text->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Ongeldige id', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Thesis->delete($id)) {
			$this->Session->setFlash(__('Tekst blok verwijderd', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Tekst blok niet verwijderd', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>