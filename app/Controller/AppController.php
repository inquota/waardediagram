<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');
App::uses('AppUsersController', 'Controller');


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller 
{
	/**
	 * @var array
	 *
	 */ 
	public $components = array('Session', 'Auth');
	
	/**
	 * beforeFilter
	 *
	 * Setup all the Auth settings here
	 */
	public function beforeFilter()
	{
		// Setup Auth
		$this->Auth->loginAction = array(
            'controller' => 'users',
            'action' => 'login',
            'plugin' => false,
			'admin' => false
		);
		
		$this->Auth->authorize = array('Controller');
		
		$this->set('is_admin','');
		$this->set('is_logged_in','');
		
		if($this->Auth->user('is_admin')){
			$user=$this->Auth->user('is_admin');
			$this->set('is_admin',$user);
		}else{
			$user=$this->Auth->user();
			$this->set('is_logged_in',$user);
		}		
		
		$this->DetectOldBrowsers();
		$this->DetectLanguage();
	}
	
	/**
	 * isAuthorized
	 *
	 * Check if authorized and check admin rights
	 */
	public function isAuthorized($user) {
		// Any registered user can access public functions
        if (empty($this->request->params['admin'])) {
			return true;
        }

        // Only admins can access admin functions
        if (isset($this->request->params['admin'])) {
			if ($user['is_admin'] == 1) {
				return true;
			}
        }

	    // Default deny
	    return false;
	}
	
	/**
	 * getTextBlocks
	 * Get the text from the database by the right language
	 * @param String $label
	 * @returm String $text
	 */
	public function getTextBlocks($labels){
		$this->loadModel('Text');
		$labels=explode(',',$labels);
		//strtoupper( $this->Session->read('language') )
		if ($this->Session->check('Config.language')) {
            if($this->Session->read('Config.language')=='nld'){
            	$lang='NL';
            }elseif($this->Session->read('Config.language')=='eng'){
            	$lang='EN';
            }
        }else{
        	$lang='NL';
        }
		$text=$this->Text->find('all', array('conditions'=>array('label'=>$labels, 'language'=> $lang ) ) );
		return $text;	
	}

	/**
	 * DetectOldBrowsers
	 * 
	 * Detection when users using IE 8 or less
	 */
	public function DetectOldBrowsers(){
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') 
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') 
				|| strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8')
			){
				$this->set('old_browser_detected',1);
			}
	}
	
	/**
	 * DetectLanguage
	 * 
	 * Detect the language based on browser prefered language setting
	 */
	public function DetectLanguage(){
		if(!$this->Session->check('language_manual')){
			$lang=substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0,2);
			if($lang=='nl'){
				$this->Session->write('Config.language', 'nld');
			}elseif($lang=='en'){
				$this->Session->write('Config.language', 'eng');
			}		
		}
	}
}