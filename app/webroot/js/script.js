$(document).ready(function(){
	
	/**
	 * Kwaliteits kenmerken en gewichten
	 */
	$( "#weight_kamerprijs").change(function() {
		if( $("#weight_kamerprijs").val() ==0)
		{
			$( "#weight_kamerprijs").fadeOut('slow');
		}else if( $("#weight_kamerprijs").val() ==1){
			$( "#weight_percentage_zero_kamerprijs").hide();
			$( "#weight_percentage_pos_kamerprijs").hide();
			$( "#weight_percentage_neg_kamerprijs").fadeIn('slow');
		}else if( $("#weight_kamerprijs").val() ==2){
			$( "#weight_percentage_neg_kamerprijs").hide();
			$( "#weight_percentage_pos_kamerprijs").hide();
			$( "#weight_percentage_zero_kamerprijs").fadeIn('slow');	
		}else if( $("#weight_kamerprijs").val() ==3){
			$( "#weight_percentage_neg_kamerprijs").hide();
			$( "#weight_percentage_zero_kamerprijs").hide();
			$( "#weight_percentage_pos_kamerprijs").fadeIn('slow');	
		}
	});
	$( "#weight_reviewscore" ).change(function() {
		if( $("#weight_reviewscore").val() ==0)
		{
			$( "#weight_reviewscore").fadeOut('slow');
		}else if( $("#weight_reviewscore").val() ==1){
			$( "#weight_percentage_zero_reviewscore").hide();
			$( "#weight_percentage_pos_reviewscore").hide();
			$( "#weight_percentage_neg_reviewscore").fadeIn('slow');
		}else if( $("#weight_reviewscore").val() ==2){
			$( "#weight_percentage_neg_reviewscore").hide();
			$( "#weight_percentage_pos_reviewscore").hide();
			$( "#weight_percentage_zero_reviewscore").fadeIn('slow');	
		}else if( $("#weight_reviewscore").val() ==3){
			$( "#weight_percentage_neg_reviewscore").hide();
			$( "#weight_percentage_zero_reviewscore").hide();
			$( "#weight_percentage_pos_reviewscore").fadeIn('slow');	
		}
	});
	
	/**
	 * Start - Pagina - Wegingsfactoren ------------------------------------------------
	 */
	$( "#weight_88" ).change(function() {	
		if( $("#weight_88").val() ==0)
		{
			$( "#weight_percentage2_88" ).fadeOut('slow');
		}else{
			$( "#weight_percentage2_88" ).fadeIn('slow');
		}
	});
	
	$( "#weight_99" ).change(function() {	
		if( $("#weight_99").val() ==0)
		{
			$( "#weight_percentage2_99" ).fadeOut('slow');
		}else{
			$( "#weight_percentage2_99" ).fadeIn('slow');
		}
	});
	
	$( "#wegingsfactoren tbody tr td#target select" ).each(function( index ) {
		$( "#weight_"+ index ).change(function() {	
			if( $("#weight_"+ index).val() ==0)
			{
				$( "#weight_percentage2_"+ index ).fadeOut('slow');
			}else{
				$( "#weight_percentage2_"+ index ).fadeIn('slow');
			}
		});
	});
		
	/**
	 * Einde - Pagina - Wegingsfactoren ------------------------------------------------
	 */
	
	$( "#kwaliteitskenmerken tbody tr td#target_hotel1 select" ).each(function( index, value ) {
		$( "#kk_hotel1_"+ index ).change(function() {
			if( $("#kk_hotel1_"+ index).val() ==0)
			{
				$( "#kk_percentage_hotel1_"+ index ).fadeOut('slow');
			}else if( $("#kk_hotel1_"+ index).val() ==1){
				$( "#kk_percentage_zero_hotel1_"+ index ).hide();
				$( "#kk_percentage_pos_hotel1_"+ index ).hide();
				$( "#kk_percentage_neg_hotel1_"+ index ).fadeIn('slow');
			}else if( $("#kk_hotel1_"+ index).val() ==2){
				$( "#kk_percentage_neg_hotel1_"+ index ).hide();
				$( "#kk_percentage_pos_hotel1_"+ index ).hide();
				$( "#kk_percentage_zero_hotel1_"+ index ).fadeIn('slow');	
			}else if( $("#kk_hotel1_"+ index).val() ==3){
				$( "#kk_percentage_neg_hotel1_"+ index ).hide();
				$( "#kk_percentage_zero_hotel1_"+ index ).hide();
				$( "#kk_percentage_pos_hotel1_"+ index ).fadeIn('slow');	
			}
		});
	});
		$( "#kwaliteitskenmerken tbody tr td#target_hotel2 select" ).each(function( index, value ) {
		$( "#kk_hotel2_"+ index ).change(function() {
			if( $("#kk_hotel2_"+ index).val() ==0)
			{
				$( "#kk_percentage_hotel2_"+ index ).fadeOut('slow');
			}else if( $("#kk_hotel2_"+ index).val() ==1){
				$( "#kk_percentage_zero_hotel2_"+ index ).hide();
				$( "#kk_percentage_pos_hotel2_"+ index ).hide();
				$( "#kk_percentage_neg_hotel2_"+ index ).fadeIn('slow');
			}else if( $("#kk_hotel2_"+ index).val() ==2){
				$( "#kk_percentage_neg_hotel2_"+ index ).hide();
				$( "#kk_percentage_pos_hotel2_"+ index ).hide();
				$( "#kk_percentage_zero_hotel2_"+ index ).fadeIn('slow');	
			}else if( $("#kk_hotel2_"+ index).val() ==3){
				$( "#kk_percentage_neg_hotel2_"+ index ).hide();
				$( "#kk_percentage_zero_hotel2_"+ index ).hide();
				$( "#kk_percentage_pos_hotel2_"+ index ).fadeIn('slow');	
			}
		});
	});
		$( "#kwaliteitskenmerken tbody tr td#target_hotel3 select" ).each(function( index, value ) {
		$( "#kk_hotel3_"+ index ).change(function() {
		if( $("#kk_hotel3_"+ index).val() ==0)
			{
				$( "#kk_percentage_hotel3_"+ index ).fadeOut('slow');
			}else if( $("#kk_hotel3_"+ index).val() ==1){
				$( "#kk_percentage_zero_hotel3_"+ index ).hide();
				$( "#kk_percentage_pos_hotel3_"+ index ).hide();
				$( "#kk_percentage_neg_hotel3_"+ index ).fadeIn('slow');
			}else if( $("#kk_hotel3_"+ index).val() ==2){
				$( "#kk_percentage_neg_hotel3_"+ index ).hide();
				$( "#kk_percentage_pos_hotel3_"+ index ).hide();
				$( "#kk_percentage_zero_hotel3_"+ index ).fadeIn('slow');	
			}else if( $("#kk_hotel3_"+ index).val() ==3){
				$( "#kk_percentage_neg_hotel3_"+ index ).hide();
				$( "#kk_percentage_zero_hotel3_"+ index ).hide();
				$( "#kk_percentage_pos_hotel3_"+ index ).fadeIn('slow');	
			}
		});
	});
		$( "#kwaliteitskenmerken tbody tr td#target_hotel4 select" ).each(function( index, value ) {
		$( "#kk_hotel4_"+ index ).change(function() {
			if( $("#kk_hotel4_"+ index).val() ==0)
			{
				$( "#kk_percentage_hotel4_"+ index ).fadeOut('slow');
			}else if( $("#kk_hotel4_"+ index).val() ==1){
				$( "#kk_percentage_zero_hotel4_"+ index ).hide();
				$( "#kk_percentage_pos_hotel4_"+ index ).hide();
				$( "#kk_percentage_neg_hotel4_"+ index ).fadeIn('slow');
			}else if( $("#kk_hotel4_"+ index).val() ==2){
				$( "#kk_percentage_neg_hotel4_"+ index ).hide();
				$( "#kk_percentage_pos_hotel4_"+ index ).hide();
				$( "#kk_percentage_zero_hotel4_"+ index ).fadeIn('slow');	
			}else if( $("#kk_hotel4_"+ index).val() ==3){
				$( "#kk_percentage_neg_hotel4_"+ index ).hide();
				$( "#kk_percentage_zero_hotel4_"+ index ).hide();
				$( "#kk_percentage_pos_hotel4_"+ index ).fadeIn('slow');	
			}
		});
	});
	$( "#kwaliteitskenmerken tbody tr td#target_hotel5 select" ).each(function( index, value ) {
		$( "#kk_hotel5_"+ index ).change(function() {
			if( $("#kk_hotel5_"+ index).val() ==0)
			{
				$( "#kk_percentage_hotel5_"+ index ).fadeOut('slow');
			}else if( $("#kk_hotel5_"+ index).val() ==1){
				$( "#kk_percentage_zero_hotel5_"+ index ).hide();
				$( "#kk_percentage_pos_hotel5_"+ index ).hide();
				$( "#kk_percentage_neg_hotel5_"+ index ).fadeIn('slow');
			}else if( $("#kk_hotel5_"+ index).val() ==2){
				$( "#kk_percentage_neg_hotel5_"+ index ).hide();
				$( "#kk_percentage_pos_hotel5_"+ index ).hide();
				$( "#kk_percentage_zero_hotel5_"+ index ).fadeIn('slow');	
			}else if( $("#kk_hotel5_"+ index).val() ==3){
				$( "#kk_percentage_neg_hotel5_"+ index ).hide();
				$( "#kk_percentage_zero_hotel5_"+ index ).hide();
				$( "#kk_percentage_pos_hotel5_"+ index ).fadeIn('slow');	
			}
		});
	});
});

// Numeric only control handler
jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};
$(".ForceNumericOnly").ForceNumericOnly();