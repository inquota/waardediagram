<?php
/**
 * Application UserController
 *
 *
 * @package       app.AppUser
 * @since         
 */

App::uses('User', 'Users.Model');

/**
 * AppUser Model
 *
 * Extending the Users Plugin.
 *
 * @package		app.AppUser
 */
class AppUser extends User 
{
	/**
	 *
	 * @var string $name
	 */
	public $name = 'AppUser';
	
	/**
	 *
	 * @var string $useTable
	 */
	public $useTable = 'users';
}
