<?php echo $this->Form->create('Page', array('class'=>'form-segments')); ?>
<div class="container">
	<div class="row">
		
		<div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
			<ol class="breadcrumb">
			  <?php if($this->Session->read('PageData')) : ?>
		  		<li><a href="/segments/choose/" title="Ga naar <?php echo __("Basisinformatie");?>"><?php echo __("Basisinformatie");?></a></li>
		 	  <?php endif; ?>
		 	  
			  <?php if($this->Session->read('Session1')) : ?>
			  	<li><a href="/segments/step/1/" title="Ga naar <?php echo __("Prijskenmerken");?>"><?php echo __("Prijskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session2')) : ?>
			  	<li><a href="/segments/step/2/" title="Ga naar <?php echo __("Kwaliteitskenmerken");?>"><?php echo __("Kwaliteitskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session3')) : ?>
			  	<li><a href="/segments/step/3/" title="Ga naar <?php echo __("Wegingsfactoren");?>"><?php echo __("Wegingsfactoren");?></a></li>
			  <?php endif; ?>
			</ol>
		</div>	
		
      <div class="col-md-offset-1 col-xs-6 col-sm-10 col-lg-11">
        <div class="well well-sm">
        	<?php echo utf8_decode($text_blocks[0]); ?>			
			  <table class="table" id="wegingsfactoren">
			        <thead>
			            <tr>
			                <th><?php echo __("Kwaliteitskenmerken");?></th>
			                <th width="130"><?php echo __("Gewicht");?></th>
			                <th width="220"><?php echo __("Gewicht (in percentage)");?></th>
			            </tr>
			        </thead>
			        <tbody>
						<tr>
			                <td><?php echo __("Kamerprijs");?></td>
			                <td id="target">
							<?php
		                	echo $this->Form->input('wegingsfactor_kenmerk_kamerprijs', array(
							    'options' => array(0=>__("kies"), 1=>__("laag"), 2=>__("gemiddeld"), 3=>__("hoog")),
							    'class' => 'form-control col-xs-1',
							    'id'=> 'weight_0',
							    'label'=> false,
							    'div'=>false
							));
		                	?>
			                </td>
			                <td>
			                	<?php echo $this->App->GenerateDropdownTo(0,$session3); ?>
							</td>
			            </tr>
			            <tr>
			                <td><?php echo __("Review score");?></td>
			                <td id="target">
							<?php
		                	echo $this->Form->input('wegingsfactor_kenmerk_reviewscore', array(
							    'options' => array(0=>__("kies"), 1=>__("laag"), 2=>__("gemiddeld"), 3=>__("hoog")),
							    'class' => 'form-control col-xs-1',
							    'id'=> 'weight_1',
							    'label'=> false,
							    'div'=>false
							));
		                	?>
			                <td>
			                	<?php echo $this->App->GenerateDropdownTo(1,$session3); ?>
							</td>
			            </tr>
			        	<?php 
			        	$l=2;
			        	foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
			            <tr>
			                <td><?php echo $kenmerk; ?></td>
			                <td id="target">
			                	<?php
			                	echo $this->Form->input('wegingsfactor_kenmerk'.$l, array(
								    'options' => array(0=>__("kies"), 1=>__("laag"), 2=>__("gemiddeld"), 3=>__("hoog")),
								    'class' => 'form-control col-md-1',
								    'id'=> 'weight_'.$l,
								    'label'=> false,
								    'div'=>false
								));
			                	?>
			                </td>
			                <td>
			                	<?php echo $this->App->GenerateDropdownTo($l,$session3); ?>
							</td>
			            </tr>
			            <?php
			            $l++; 
			            endforeach; ?>
			            <tr>
			            <td colspan="2">&nbsp;</td>
			            <td>
			            	<?php if(isset($session3)) : ?>
								<span id="total" style="font-size: 1.4em;">100</span>
							<?php else : ?>
								<span id="total" style="font-size: 1.4em;">0</span>
							<?php endif; ?>			            	
			            	<span style="font-size: 1.4em;">%</span></td>
			            </tr>
			        </tbody>
			    </table>
			    <?php echo utf8_decode($text_blocks[1]); ?>			   
			   <div id="btn-container">
			    
			    </div>
			    
        </div>
      </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>


<script>
$(document).ready(function(){
	var selects = $('select[name^=dropdown_weight2]');
	selects.change(function(){
	    var value = 0;
	    selects.each(function(){ value += +this.value; });
		 if(value > 100){
			 alert('<?php echo __("Het percentage moet op 100% uitkomen. U komt nu op");?>' + value +"%");
		 }else if(value == 100){
			 $("#btn-container").html('<button type="submit" class="btn btn-lg btn-primary"><?php echo __("Opslaan");?></button>');    
		 }else{
			 $('#total').html(value);
		 }
	   
	}).trigger('change');
});
</script>