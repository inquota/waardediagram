<?php echo $this->Form->create('Page', array('class'=>'form-segments')); ?>
<div class="container">
	<div class="row">
		
		<div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
			<ol class="breadcrumb">
			  <?php if($this->Session->read('PageData')) : ?>
		  		<li><a href="/segments/choose/" title="Ga naar <?php echo __("Basisinformatie");?>"><?php echo __("Basisinformatie");?></a></li>
		 	  <?php endif; ?>
		 	  
			  <?php if($this->Session->read('Session1')) : ?>
			  	<li><a href="/segments/step/1/" title="Ga naar <?php echo __("Prijskenmerken");?>"><?php echo __("Prijskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session2')) : ?>
			  	<li><a href="/segments/step/2/" title="Ga naar <?php echo __("Kwaliteitskenmerken");?>"><?php echo __("Kwaliteitskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session3')) : ?>
			  	<li><a href="/segments/step/3/" title="Ga naar <?php echo __("Wegingsfactoren");?>"><?php echo __("Wegingsfactoren");?></a></li>
			  <?php endif; ?>
			</ol>
		</div>	
		
      <div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
        <div class="well well-sm">
        
		<?php echo $text_blocks[0]; ?>

	    <table class="table">
	        <thead>
	            <tr>
	                <th width="200"><?php echo __("Verloop van het prijsniveau");?>	</th>
	                <th width="100"><?php echo $hotel_name; ?></th>
					<?php for($i=1; $i <= $HotelAmount; $i++) : ?>
	                	<th><?php echo $PageData['Page']['abrv'.$i]; ?></th>
	                <?php endfor; ?>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td><?php echo __("Gemiddelde kamerprijs");?></td>	                
	                <td><?php echo $this->Form->input('prijsniveau_own_hotel1', array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	                <?php for($i=1; $i <= $HotelAmount; $i++) : ?>
	                <td><?php echo $this->Form->input('prijsniveau_hotel_gemiddeld'.$i, array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	                <?php endfor; ?>
	            </tr>
	            <tr>
	                <td><?php echo __("Hoogste kamerprijs");?>	</td>
	                <td><?php echo $this->Form->input('prijsniveau_own_hotel2', array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	          		<?php for($i=1; $i <= $HotelAmount; $i++) : ?>
	                <td><?php echo $this->Form->input('prijsniveau_hotel_hoogste'.$i, array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	                <?php endfor; ?>
	            </tr>
	            <tr>
	                <td><?php echo __("Laagste kamerprijs");?>	</td>
	                <td><?php echo $this->Form->input('prijsniveau_own_hotel3', array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	         		 <?php for($i=1; $i <= $HotelAmount; $i++) : ?>
	                <td><?php echo $this->Form->input('prijsniveau_hotel_laagste'.$i, array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
	                <?php endfor; ?>
	            </tr>
	        </tbody>
	    </table>
     	<?php echo $this->Form->button(__("Opslaan"), array('type' => 'submit', 'class'=> 'btn btn-lg btn-primary')); ?>
  
        </div>
      </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>