<?php echo $this->Form->create('Page', array('class'=>'form-segments')); ?>
<div class="container">
	<div class="row">
		
		<div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
			<ol class="breadcrumb">
			  <?php if($this->Session->read('PageData')) : ?>
		  		<li><a href="/segments/choose/" title="Ga naar <?php echo __("Basisinformatie");?>"><?php echo __("Basisinformatie");?></a></li>
		 	  <?php endif; ?>
		 	  
			  <?php if($this->Session->read('Session1')) : ?>
			  	<li><a href="/segments/step/1/" title="Ga naar <?php echo __("Prijskenmerken");?>"><?php echo __("Prijskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session2')) : ?>
			  	<li><a href="/segments/step/2/" title="Ga naar <?php echo __("Kwaliteitskenmerken");?>"><?php echo __("Kwaliteitskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session3')) : ?>
			  	<li><a href="/segments/step/3/" title="Ga naar <?php echo __("Wegingsfactoren");?>"><?php echo __("Wegingsfactoren");?></a></li>
			  <?php endif; ?>
			</ol>
		</div>	
		
	<div class="col-md-offset-1 col-xs-12 col-sm-10 col-lg-11">
		<div class="well well-sm">
        	<?php echo utf8_decode($text_blocks[0]); ?>
        	
        	<h3><?php echo __("Ten aanzien van segment: %s",$PageData['Page']['segment1']);?></h3>
			<table class="table">
			    <thead>
			        <tr>
			            <th><?php echo __("Review Score");?></th>
			            <th><?php echo $hotel_name; ?></th>
			            <th>&nbsp;</th>
						<?php for($i=1; $i <= $HotelAmount; $i++) : ?>
			            	<th><?php echo $PageData['Page']['abrv'.$i]; ?></th>
			            <?php endfor; ?>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td width="170">
			            	<?php $reviewscorehotels= array('Booking.com'=>__("Booking.com"), 'Tripadvisor'=>__("Tripadvisor")); ?>
			            	<select name="reviewscorehotels" class="form-control">
			            		<?php foreach($reviewscorehotels as $j=>$reviewscorehotel) : ?>
			            			<option value="<?php echo $j; ?>"><?php echo $reviewscorehotel; ?></option>
			            		<?php endforeach; ?>	
			            	</select>
			            </td>			            
			            <td><?php echo $this->Form->input('review_score_uw_hotel', array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
			            <td>&nbsp;</td>            
						<?php for($i=1; $i <= $HotelAmount; $i++) : ?>
			            	<td><?php echo $this->Form->input('reviewscores_hotel'.$i, array('class'=>'form-control ForceNumericOnly', 'label'=> false, 'required'=>'required'));  ?></td>
			            <?php endfor; ?>
			        </tr>
			    </tbody>
			</table>

        </div>
      </div> 

      <div class="col-md-offset-1 col-xs-12 col-sm-10 col-lg-11 mt30">
        <div class="well well-sm">
			<?php echo utf8_decode($text_blocks[1]); ?>
			
			<h3><?php echo __("Ten aanzien van segment: %s",$PageData['Page']['segment1']);?></h3>
			<table class="table" id="kwaliteitskenmerken">
		        <thead>
		            <tr>
		                <th><?php echo __("Kwaliteitskenmerken");?></th>
		                <th><?php echo $hotel_name; ?></th>
		                <th>&nbsp;</th>	            
						<?php for($i=1; $i <= $HotelAmount; $i++) : ?>
		                	<th colspan="2"><?php echo $PageData['Page']['abrv'.$i]; ?></th>
		                <?php endfor; ?>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php 
		        	$j=1;
		        	foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
		            <tr>		                
		                <td><?php echo $this->Form->input('kwaliteitskenmerk'.$j, array('class'=>'form-control col-xs-6 col-sm-6 col-lg-6"', 'label'=> false, 'required'=>'required', 'value'=>$kenmerk));  ?></td>
		                <td>100</td>
		                <td>&nbsp;</td>
		                
		                <?php if(1 <= $HotelAmount) : ?>
		                <td id="target_hotel1">
		           			<?php
		                	echo $this->Form->input('hotel1_kenmerk'.$j, array(
							    'options' => array(0=>__("kies"), 1=>__("slechter"), 2=>__("zelfde"), 3=>__("beter")),
							    'class' => 'form-control',
							    'id'=> 'kk_hotel1_'.$k,
							    'label'=> false,
							    'div'=>false							    
							));
		                	?>
		                </td>
		                <td>	
		                	<?php $kenmerken_merged_final1= ($kenmerken_merged) ? $kenmerken_merged[0] : null ; ?>
		                	<?php echo $this -> App -> GenerateDropDownKenmerken($aPercent_negative, $aPercent_positive, 'hotel1', $k, $session2, $kenmerken_merged_final1); ?>
						</td>
						<?php endif; ?>
						
						<?php if(2 <= $HotelAmount) : ?>
		                <td id="target_hotel2">
				           	<?php
		                	echo $this->Form->input('hotel2_kenmerk'.$j, array(
							    'options' => array(0=>__("kies"), 1=>__("slechter"), 2=>__("zelfde"), 3=>__("beter")),
							    'class' => 'form-control',
							    'id'=> 'kk_hotel2_'.$k,
							    'label'=> false,
							    'div'=>false
							));
		                	?>
		                </td>
						<td>
							<?php $kenmerken_merged_final2= ($kenmerken_merged) ? $kenmerken_merged[1] : null ; ?>
		      				<?php echo $this -> App -> GenerateDropDownKenmerken($aPercent_negative, $aPercent_positive, 'hotel2', $k, $session2, $kenmerken_merged_final2); ?>
						</td>
						<?php endif; ?>
						
						
						<?php if(3 <= $HotelAmount) : ?>
		                <td id="target_hotel3">
		           			<?php
		                	echo $this->Form->input('hotel3_kenmerk'.$j, array(
							    'options' => array(0=>__("kies"), 1=>__("slechter"), 2=>__("zelfde"), 3=>__("beter")),
							    'class' => 'form-control',
							    'id'=> 'kk_hotel3_'.$k,
							    'label'=> false,
							    'div'=>false
							));
		                	?>
		                </td>
						<td>
							<?php $kenmerken_merged_final3= ($kenmerken_merged) ? $kenmerken_merged[2] : null ; ?>
		    				<?php echo $this -> App -> GenerateDropDownKenmerken($aPercent_negative, $aPercent_positive, 'hotel3', $k, $session2, $kenmerken_merged_final3); ?>
						</td>
						<?php endif; ?>
						
						<?php if(4 <= $HotelAmount) : ?>
		                <td id="target_hotel4">
		           			<?php
		                	echo $this->Form->input('hotel4_kenmerk'.$j, array(
							    'options' => array(0=>__("kies"), 1=>__("slechter"), 2=>__("zelfde"), 3=>__("beter")),
							    'class' => 'form-control',
							    'id'=> 'kk_hotel4_'.$k,
							    'label'=> false,
							    'div'=>false
							));
		                	?>
		                </td>
						<td>
							<?php $kenmerken_merged_final4= ($kenmerken_merged) ? $kenmerken_merged[3] : null ; ?>
		                	<?php echo $this -> App -> GenerateDropDownKenmerken($aPercent_negative, $aPercent_positive, 'hotel4', $k, $session2, $kenmerken_merged_final4); ?>
						</td>
						<?php endif; ?>
						
						<?php if(5 <= $HotelAmount) : ?>
		                <td id="target_hotel5">
		           			<?php
		                	echo $this->Form->input('hotel5_kenmerk'.$j, array(
							    'options' => array(0=>__("kies"), 1=>__("slechter"), 2=>__("zelfde"), 3=>__("beter")),
							    'class' => 'form-control',
							    'id'=> 'kk_hotel5_'.$k,
							    'label'=> false,
							    'div'=>false
							));
		                	?>
		                </td>
						<td>
							<?php $kenmerken_merged_final5= ($kenmerken_merged) ? $kenmerken_merged[4] : null ; ?>
		                	<?php echo $this -> App -> GenerateDropDownKenmerken($aPercent_negative, $aPercent_positive, 'hotel5', $k, $session2, $kenmerken_merged_final5); ?>
						</td>
						<?php endif; ?>
						
		            </tr>
		            <?php 
		            $j++;
		            endforeach; ?>
		        </tbody>
		    </table>
        	<?php echo $this->Form->button(__("Opslaan"), array('type' => 'submit', 'class'=> 'btn btn-lg btn-primary')); ?>
   
			</div>
		</div>
	</div>
</div>
<?php echo $this -> Form -> end(); ?>

<script>
	/*$(document).ready(function(){
		$("select > option[value*=2]").remove();
	});*/
</script>
