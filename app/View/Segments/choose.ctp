<?php echo $this->Form->create('Page', array('class'=>'form-segments')); ?>
<div class="container">
	<div class="row">
		
		<div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
			<ol class="breadcrumb">
			  <?php if($this->Session->read('PageData')) : ?>
		  		<li><a href="/segments/choose/" title="Ga naar <?php echo __("Basisinformatie");?>"><?php echo __("Basisinformatie");?></a></li>
		 	  <?php endif; ?>
		 	  
			  <?php if($this->Session->read('Session1')) : ?>
			  	<li><a href="/segments/step/1/" title="Ga naar <?php echo __("Prijskenmerken");?>"><?php echo __("Prijskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session2')) : ?>
			  	<li><a href="/segments/step/2/" title="Ga naar <?php echo __("Kwaliteitskenmerken");?>"><?php echo __("Kwaliteitskenmerken");?></a></li>
			  <?php endif; ?>
			  
			  <?php if($this->Session->read('Session3')) : ?>
			  	<li><a href="/segments/step/3/" title="Ga naar <?php echo __("Wegingsfactoren");?>"><?php echo __("Wegingsfactoren");?></a></li>
			  <?php endif; ?>
			</ol>
		</div>	
		
      <div class="col-md-offset-1 col-xs-12 col-sm-6 col-lg-11">
        <div class="well well-sm">
		
		
		<?php echo $blocks[0]['Text']['tekst']; ?>
		
		  <table class="table">
	        <thead>
	            <tr>
	                <th><?php echo __("De volledige naam van uw hotel");?></th>
	                <th><?php echo __("Korte naam");?></th>
	                <th><?php echo __("Afkorting");?></th>
	                <th>&nbsp;</th>
	                <th><?php echo __("Aantal kamers");?></th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td width="190"><?php echo $hotel_name; ?></td>
	                <td width="110"><?php echo $this->Form->input('own_name_short', array('class'=>'form-control', 'label'=> false, 'required'=>'required')); ?></td>
	                <td width="70"><?php echo $this->Form->input('own_abvr', array('class'=>'form-control', 'label'=> false, 'required'=>'required'));  ?></td>
	                <td width="60">&nbsp;</td>
	                <td width="10"><?php echo $this->Form->input('own_rooms', array('class'=>'form-control', 'label'=> false, 'required'=>'required'));  ?></td>
	            </tr>
	        </tbody>
	    </table>
		   
	   <table class="table">
	        <thead>
	            <tr>
	                <th><?php echo __("De volledige naam van uw hotel");?></th>
	                <th><?php echo __("Korte naam");?></th>
	                <th><?php echo __("Afkorting");?></th>
	                <th>&nbsp;</th>
	                <th><?php echo __("Aantal kamers");?></th>
	            </tr>
	        </thead>
	        <tbody>
	            <?php for($i=1;$i < 6; $i++) : ?>
	            <tr>
	                <td width="190"><?php echo $this->Form->input('name_full'.$i, array('class'=>'form-control', 'label'=> false, 'placeholder'=> $i . '.'));  ?></td>
	                <td width="110"><?php echo $this->Form->input('name_short'.$i, array('class'=>'form-control maxchars20', 'label'=> false));  ?></td>
	                <td width="70"><?php echo $this->Form->input('abrv'.$i, array('class'=>'form-control abrv maxchars3', 'label'=> false));  ?></td>
	                <td width="60">&nbsp;</td>
	                <td width="10"><?php echo $this->Form->input('rooms'.$i, array('class'=>'form-control ForceNumericOnly maxchars3', 'label'=> false));  ?></td>
	            </tr>
	            <?php endfor; ?>
	        </tbody>
	    </table>
	   
		<p>
			<?php echo $blocks[1]['Text']['tekst']; ?>
		</p>

	    <table class="table  col-md-2">
	        <thead>
	            <tr>
	                <th><?php echo __("Korte naam voor uw segmenten");?></th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td><?php echo $this->Form->input('segment1', array('class'=>'form-control', 'placeholder'=>__("Leisure (voorbeeld)"), 'label'=> false, 'required'=>'required'));  ?></td>
	            </tr>
	        </tbody>
	    </table>
  
    	<?php echo $this->Form->button(__("Opslaan"), array('type' => 'submit', 'class'=> 'btn btn-lg btn-primary')); ?>
        </div>
      </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<script>
$( ".maxchars3" ).keydown(function( event ) {
	if ( $(this).val().length > 3 ) {
		var value=$(this).val();
		$(this).val( value.substring(0,2) );
	}
});

$( ".maxchars20" ).keydown(function( event ) {
	if ( $(this).val().length > 20 ) {
		var value=$(this).val();
		$(this).val( value.substring(0,19) );
	}
});
</script>
