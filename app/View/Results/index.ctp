<?php
$e124_32='';

$i=1;
$j=0;
?>

<div class="container">
	<div class="row">
      <div class="col-xs-12 col-sm-6 col-lg-11">
        <div class="well well-sm">
        
         <!-- Accordion menu -->	
          <div class="panel-group" id="accordion">
          	
          	<?php foreach($segments as $k=>$segment) : ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                <?php 
                if( count($segments) > 1) : ?>
            		<?php if($i==2 || $i==3) : ?>
            				<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse<?php echo strtoupper($k); ?>"><?php echo $segment; ?></a>
            			<?php else : ?>
            				<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo strtoupper($k); ?>"><?php echo $segment; ?></a>
            		<?php endif; ?>	
            	<?php else : ?>	
            		<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo strtoupper($k); ?>"><?php echo $segment; ?></a>
                <?php endif; ?>	                
                </h4>
              </div>
              
             <?php if( count($segments) > 1) : ?>
	        		<?php if($i==2 || $i==3) : ?>
	        			<div id="collapse<?php echo strtoupper($k); ?>" class="panel-collapse collapse">
	        		<?php else : ?>
	        			<div id="collapse<?php echo strtoupper($k); ?>" class="panel-collapse collapse in">
	        		<?php endif; ?>
	        	<?php else : ?>	
	        		<div id="collapse<?php echo strtoupper($k); ?>" class="panel-collapse collapse in">
               <?php endif; ?>	
              
                <div class="panel-body">
					
		        <!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
				  <li class="active"><a href="#profile<?php echo $j; ?>" role="tab" data-toggle="tab">Tabel</a></li>
				  <li><a href="#messages<?php echo $j; ?>" role="tab" data-toggle="tab">Grafiek</a></li>
				  <li><a href="#settings<?php echo $j; ?>" role="tab" data-toggle="tab">Scoretabel</a></li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane active" id="profile<?php echo $j; ?>">
				   <table class="table-striped table">
					<tr>
						<td>&nbsp;</td>
						<td>Rescaled gewicht</td>
						<td>&nbsp;</td>
						<td>Uw hotel</td>
						<?php foreach($hotels[$j] as $hotel) : ?>
						<td colspan="2"><?php echo $hotel; ?></td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td>Gemiddelde kamerprijs</td>
						<td><?php $c122=($waardekenmerk_kamerprijs[0]/$waardekenmerk_kamerprijs[0]*100); echo $c122; ?>%</td>
						<td><?php echo $d122; ?></td>
						<td><?php $e122=($d122*$c122)/100; echo $e122; ?></td>
								<?php foreach($average_room_prices as $room_price) : ?>
						<td><?php echo $room_price; ?></td>
						<td><?php echo ($c122*$room_price)/100; ?></td>
						<?php endforeach; ?>
					</tr>
					<?php foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
						<?php $border_bottom= ($k==0) ? 'style="border-bottom: 1px solid #000;"' : ''; ?>
					<tr>
						<td <?= $border_bottom; ?>><?php echo $kenmerk; ?></td>
						<td <?= $border_bottom; ?>><?php echo $RescaledWeights[$k]; ?>%</td>
						
						<?php if($k==0) : ?>
							<td <?= $border_bottom; ?>>100</td>
							<td <?= $border_bottom; ?>><?php $e123= ($RescaledWeights[0]*100/100); echo $e123; ?></td>
							
						<?php else : ?>
							<td <?= $border_bottom; ?>><?php echo $segmenten['Uw hotel'][$kenmerk]; ?></td>
							<td <?= $border_bottom; ?>><?php $e124_32=($RescaledWeights[$k]*$kwaliteitskenmerken_uw_hotel[$k]/100); echo $e124_32; $e124_32_array[]=$e124_32; ?></td>
						<?php endif; ?>
						
						<td <?= $border_bottom; ?>><?php echo $segmenten[$hotels[$j][0]][$kenmerk]; ?></td>
						<td <?= $border_bottom; ?>><?php echo number_format( ( ($e124_32*$segmenten[$hotels[$j][0]][$kenmerk])/100),0); ?></td>
						<td <?= $border_bottom; ?>><?php echo $segmenten[$hotels[$j][1]][$kenmerk]; ?></td>
						<td <?= $border_bottom; ?>><?php echo number_format( ( ($e124_32*$segmenten[$hotels[$j][1]][$kenmerk])/100),0); ?></td>
						<td <?= $border_bottom; ?>><?php echo $segmenten[$hotels[$j][2]][$kenmerk]; ?></td>
						<td <?= $border_bottom; ?>><?php echo number_format( ( ($e124_32*$segmenten[$hotels[$j][2]][$kenmerk])/100),0); ?></td>
						<td <?= $border_bottom; ?>><?php echo $segmenten[$hotels[$j][3]][$kenmerk]; ?></td>
						<td <?= $border_bottom; ?>><?php echo number_format( ( ($e124_32*$segmenten[$hotels[$j][3]][$kenmerk])/100),0); ?></td>
						<td <?= $border_bottom; ?>><?php echo $segmenten[$hotels[$j][4]][$kenmerk]; ?></td>
						<td <?= $border_bottom; ?>><?php echo number_format( ( ($e124_32*$segmenten[$hotels[$j][4]][$kenmerk])/100),0); ?></td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Uw hotel</td>
						<?php foreach($hotels[$j] as $hotel) : ?>
						<td colspan="2"><?php echo $hotel; ?></td>
						<?php endforeach; ?>
					</tr>
						<tr>
						<td>GLOBAAL (Y-as)</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php $e134= ($e122/10)-10; echo $e134; ?></td>
						<?php foreach($CoordsyAxis as $yAxis) : ?>
							<td colspan="2"><?php echo $yAxis; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td>GLOBAAL (X-as)</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php $e135= ( (array_sum($e124_32_array)+$e123) /10)-10; echo $e135; ?></td>
						<?php foreach($CoordsxAxis as $xAxis) : ?>
							<td colspan="2"><?php echo $xAxis; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td>GLOBAAL (netto)</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php echo $e135-$e134; ?></td>
						<?php foreach($this->App->CoordsNetto($CoordsxAxis,$CoordsyAxis) as $netto) : ?>
							<td colspan="2"><?php echo $netto; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td>Aantal kamers</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php echo $own_hotel_rooms; ?></td>
						<?php foreach($hotel_rooms as $hotel_room) : ?>
							<td colspan="2"><?php echo $hotel_room; ?></td>
						<?php endforeach; ?>	
					</tr>
				</table>
				  </div>
				  <div class="tab-pane" id="messages<?php echo $j; ?>">
					<div id="container-chart<?php echo $j; ?>" style="height: 600px; min-width: 510px; width: 960px; max-width: 1020px; margin: 0 auto"></div>
					
					<div id="controls<?php echo $j; ?>">		
					
					<table class="table-striped table">
					<tr>
						<td>&nbsp;</td>
						<td>X-as</td>
						<td>Y-as</td>
						<td>Kamers</td>
					</tr>
					<tr>
						<td>Uw hotel</td>
						<td><?php echo $e135; ?></td>
						<td><?php echo $e134; ?></td>
						<td><?php echo $own_hotel_rooms; ?></td>
					</tr>
					<?php foreach($hotels[$j] as $k=>$hotel) : ?>
					<tr>
						<td width="100"><?php echo $hotel; ?></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $CoordsxAxis[$k]; ?>" id="hotel<?php echo $j; ?><?php echo $k; ?>_x" /></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $CoordsyAxis[$k]; ?>" data-fgColor="#66CC66" id="hotel<?php echo $j; ?><?php echo $k; ?>_y"  /></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="0" data-max="300" data-displayPrevious=true value="<?php echo $hotel_rooms[$k]; ?>" data-fgColor="#ffec03" id="hotel<?php echo $j; ?><?php echo $k; ?>_rooms" /></td>
					</tr>
					<?php endforeach; ?>
					</table>							
					</div>
					
				  </div>
				  <div class="tab-pane" id="settings<?php echo $j; ?>">
					  <table class="table-striped table">
						<tr>
							<td>&nbsp;</td>
							<td>Uw hotel</td>
							<?php foreach($hotels[$j] as $hotel) : ?>
							<td colspan="2"><?php echo $hotel; ?></td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td>Marktpositie t.a.v. kamerprijs</td>
							<td>= benchmark</td>
									<?php foreach($average_room_prices as $room_price) : ?>
							<td><?php $benchmark_room_price= abs( ($room_price - $d122) / 100 * 100); echo $benchmark_room_price; ?>%</td>
							<td><?php echo $this->App->GetBeterOfSlechter($benchmark_room_price); ?></td>
							<?php endforeach; ?>
						</tr>
						<?php foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
							<?php $border_bottom= ($k==0) ? 'style="border-bottom: 1px solid #000;"' : ''; ?>
						<tr>
							<td <?= $border_bottom; ?>>Marktpositie t.a.v. <?php echo $kenmerk; ?></td>
							<td <?= $border_bottom; ?>>= benchmark</td>
							<td <?= $border_bottom; ?>><?php $benchmark_hotel1= abs( ($segmenten[$hotels[$j][0]][$kenmerk] - $review_scores[0]) / 100 * 100); echo $benchmark_hotel1; ?>%</td>
							<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel1); ?></td>
							<td <?= $border_bottom; ?>><?php $benchmark_hotel2= abs( ($segmenten[$hotels[$j][1]][$kenmerk] - $review_scores[0]) / 100 * 100); echo $benchmark_hotel2; ?>%</td>
							<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel2); ?></td>
							<td <?= $border_bottom; ?>><?php $benchmark_hotel3= abs( ($segmenten[$hotels[$j][2]][$kenmerk] - $review_scores[0]) / 100 * 100); echo $benchmark_hotel3; ?>%</td>
							<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel3); ?></td>
							<td <?= $border_bottom; ?>><?php $benchmark_hotel4= abs( ($segmenten[$hotels[$j][3]][$kenmerk] - $review_scores[0]) / 100 * 100); echo $benchmark_hotel4; ?>%</td>
							<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel4); ?></td>
							<td <?= $border_bottom; ?>><?php $benchmark_hotel5= abs( ($segmenten[$hotels[$j][4]][$kenmerk] - $review_scores[0]) / 100 * 100); echo $benchmark_hotel5; ?>%</td>
							<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel5); ?></td>
						</tr>
						<?php endforeach; ?>
						<tr>
							<td>&nbsp;</td>
							<td>5</td>
							<?php foreach($hotels[$j] as $hotel) : ?>
							<td colspan="2"><?php echo $hotel; ?></td>
							<?php endforeach; ?>
						</tr>
					</table>
				  </div>
				</div>
		        
		        
		        
		        
                </div>
              </div>
              
              <script>
				$(document).ready(function() {
					
					// Update HighCharts with the jQuery Knob
					$(".knob").knob({
					    change: function (value) {        
					        // Retrieve all input from form
							var hotel<?php echo $j; ?>0_x = $("#hotel<?php echo $j; ?>0_x").val();
							var hotel<?php echo $j; ?>1_x = $("#hotel<?php echo $j; ?>1_x").val();
							var hotel<?php echo $j; ?>2_x = $("#hotel<?php echo $j; ?>2_x").val();
							var hotel<?php echo $j; ?>3_x = $("#hotel<?php echo $j; ?>3_x").val();
							var hotel<?php echo $j; ?>4_x = $("#hotel<?php echo $j; ?>4_x").val();
							
							var hotel<?php echo $j; ?>0_y = $("#hotel<?php echo $j; ?>0_y").val();
							var hotel<?php echo $j; ?>1_y = $("#hotel<?php echo $j; ?>1_y").val();
							var hotel<?php echo $j; ?>2_y = $("#hotel<?php echo $j; ?>2_y").val();
							var hotel<?php echo $j; ?>3_y = $("#hotel<?php echo $j; ?>3_y").val();
							var hotel<?php echo $j; ?>4_y = $("#hotel<?php echo $j; ?>4_y").val();
							
							var hotel<?php echo $j; ?>0_rooms = $("#hotel<?php echo $j; ?>0_rooms").val();
							var hotel<?php echo $j; ?>1_rooms = $("#hotel<?php echo $j; ?>1_rooms").val();
							var hotel<?php echo $j; ?>2_rooms = $("#hotel<?php echo $j; ?>2_rooms").val();
							var hotel<?php echo $j; ?>3_rooms = $("#hotel<?php echo $j; ?>3_rooms").val();
							var hotel<?php echo $j; ?>4_rooms = $("#hotel<?php echo $j; ?>4_rooms").val();
					 	        
					        $('#container-chart<?php echo $j; ?>').highcharts({
					
							    chart: {
							        type: 'bubble',
							        zoomType: 'xy',
							        animation: false
							    },
						
							    title: {
							    	text: 'Marktpositie Analyse Tool'
							    },
						        
						      yAxis: {
									min: (Math.min(hotel<?php echo $j; ?>0_y, hotel<?php echo $j; ?>1_y, hotel<?php echo $j; ?>2_y, hotel<?php echo $j; ?>3_y, hotel<?php echo $j; ?>4_y) - 20),
							    	max: (Math.max(hotel<?php echo $j; ?>0_y, hotel<?php echo $j; ?>1_y, hotel<?php echo $j; ?>2_y, hotel<?php echo $j; ?>3_y, hotel<?php echo $j; ?>4_y) + 20),
							    		    	lineWidth:1,
						        gridLineWidth: 0,
						            title: {
						                text:  'Mate waarin een hotel Duurder (+) or Goedkoper (-) is (y-as)'  
						            },
						                plotLines: [{
						            color: '#000',
						            width: 1,
						            value: 0
						        }] 
						    },
						
							    xAxis: {
							 		min: (Math.min(hotel<?php echo $j; ?>0_x, hotel<?php echo $j; ?>1_x, hotel<?php echo $j; ?>2_x, hotel<?php echo $j; ?>3_x, hotel<?php echo $j; ?>4_x) - 20),
							    	max: (Math.max(hotel<?php echo $j; ?>0_x, hotel<?php echo $j; ?>1_x, hotel<?php echo $j; ?>2_x, hotel<?php echo $j; ?>3_x, hotel<?php echo $j; ?>4_x) + 20),
							    	lineWidth:1,
						        gridLineWidth: 0,
						        
						            title: {
						                text:  'Mate waarin een hotel meer (+) of minder (-)  klantwaarde biedt (x-as)'  
						            },
						        plotLines: [{
						            color: '#000',
						            width: 1,
						            value: 0
						        }]    
						    },
							    series: [
											{
										    	name: 'Uw hotel',
										        data: [[ <?php echo $e134; ?>,<?php echo $e135; ?>,<?php echo $own_hotel_rooms; ?>]]
										    }, 
							    			{
										    	name: '<?php echo $hotels[$j][0]; ?>',
										        data: [[parseFloat(hotel<?php echo $j; ?>0_x),parseFloat(hotel<?php echo $j; ?>0_y),parseFloat(hotel<?php echo $j; ?>0_rooms)]]
										    }, 
										    {
										    	name: '<?php echo $hotels[$j][1]; ?>',
										        data: [[parseFloat(hotel<?php echo $j; ?>1_x),parseFloat(hotel<?php echo $j; ?>1_y),parseFloat(hotel<?php echo $j; ?>1_rooms)]]
										    }, 
										    {
										    	name: '<?php echo $hotels[$j][2]; ?>',
										        data: [[parseFloat(hotel<?php echo $j; ?>2_x),parseFloat(hotel<?php echo $j; ?>2_y),parseFloat(hotel<?php echo $j; ?>2_rooms)]]
									        },
									        {
									        	name: '<?php echo $hotels[$j][3]; ?>',
									        	data: [[parseFloat(hotel<?php echo $j; ?>3_x),parseFloat(hotel<?php echo $j; ?>3_y),parseFloat(hotel<?php echo $j; ?>3_rooms)]]
									        },
									        {
									        	name: '<?php echo $hotels[$j][4]; ?>',
									        	data: [[parseFloat(hotel<?php echo $j; ?>4_x),parseFloat(hotel<?php echo $j; ?>4_y),parseFloat(hotel<?php echo $j; ?>4_rooms)]]
									        }
										]
							
							});
					      
					    }
					    
					});
				
				
					// Load Diagram init
				     $('#container-chart<?php echo $j; ?>').highcharts({
					    chart: {
					        type: 'bubble',
					        zoomType: 'xy'
					    },
				
					    title: {
					    	text: 'Marktpositie Analyse Tool'
					    },
				        
				        	    yAxis: {
							min: <?php echo ( min($CoordsyAxis) - 20 ); ?>,
						    max: <?php echo ( max($CoordsyAxis) + 20 ); ?>,
					    		    	lineWidth:1,
				        gridLineWidth: 0,
				            title: {
				                text:  'Mate waarin een hotel Duurder (+) or Goedkoper (-) is (y-as)'  
				            },
				                plotLines: [{
				            color: '#000',
				            width: 1,
				            value: 0
				        }] 
				    },
				
					    xAxis: {
					    	min: <?php echo ( min($CoordsxAxis) - 20 ); ?>,
						    max: <?php echo ( max($CoordsxAxis) + 20 ); ?>,
					    	lineWidth:1,
				        gridLineWidth: 0,
				        
				            title: {
				                text:  'Mate waarin een hotel meer (+) of minder (-)  klantwaarde biedt (x-as)'  
				            },
				        plotLines: [{
				            color: '#000',
				            width: 1,
				            value: 0
				        }]    
				    },
					    series: [
									{
								    	name: 'Uw hotel',
								        data: [[ <?php echo $e134; ?>,<?php echo $e135; ?>,<?php echo $own_hotel_rooms; ?>]]
								    }, 
					    			{
								    	name: '<?php echo $hotels[$j][0]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[0],$CoordsyAxis[0],$hotel_rooms[0] ); ?>]]
								    }, 
								    {
								    	name: '<?php echo $hotels[$j][1]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[1],$CoordsyAxis[1],$hotel_rooms[1] ); ?>]]
								    }, 
								    {
								    	name: '<?php echo $hotels[$j][2]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[2],$CoordsyAxis[2],$hotel_rooms[2] ); ?>]]
							        },
							        {
							        	name: '<?php echo $hotels[$j][3]; ?>',
							        	data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[3],$CoordsyAxis[3],$hotel_rooms[3] ); ?>]]
							        },
							        {
							        	name: '<?php echo $hotels[$j][4]; ?>',
							        	data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[4],$CoordsyAxis[4],$hotel_rooms[4] ); ?>]]
							        }
								]
					
					});
					
				});
				
				// Default Draw function
				$(function() {
					$(".knob").knob({
					    draw : function () {
					
					        // "tron" case
					        if(this.$.data('skin') == 'tron') {
					
					            var a = this.angle(this.cv)  // Angle
					                , sa = this.startAngle          // Previous start angle
					                , sat = this.startAngle         // Start angle
					                , ea                            // Previous end angle
					                , eat = sat + a                 // End angle
					                , r = true;
					
					            this.g.lineWidth = this.lineWidth;
					
					            this.o.cursor
					                && (sat = eat - 0.3)
					                && (eat = eat + 0.3);
					
					            if (this.o.displayPrevious) {
					                ea = this.startAngle + this.angle(this.value);
					                this.o.cursor
					                    && (sa = ea - 0.3)
					                    && (ea = ea + 0.3);
					                this.g.beginPath();
					                this.g.strokeStyle = this.previousColor;
					                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
					                this.g.stroke();
					            }
					
					            this.g.beginPath();
					            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
					            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
					            this.g.stroke();
					
					            this.g.lineWidth = 2;
					            this.g.beginPath();
					            this.g.strokeStyle = this.o.fgColor;
					            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
					            this.g.stroke();
					
					            return false;
					        }
					    }
					});
				});
				</script>



                      <?php 
                      $j++;
                      $i++; ?>
            <?php endforeach; ?>
            </div>
    

          </div>  
         <!-- Accordion menu -->
     
    
		</div>
	</div>
</div>