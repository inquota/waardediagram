<?php
$e124_32='';
?>

<div class="container">
	<div class="row">
      <div class="col-xs-12 col-sm-6 col-lg-11">
      	
      	<?php echo $text_blocks[0]['Text']['tekst']; ?>
      	
      	<div class="btn-group btn-breadcrumb">
			<?php foreach($segments as $k=>$value) : ?>
     			<a href="/results/view/<?php echo $k; ?>/" class="btn btn-default"><?php echo $value; ?></a>
     		<?php endforeach; ?>
     		
     		<?php if( count($segments) < 3 ) : ?>
     			<a href="/segments/restart/" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> <?php echo __("Segment toevoegen");?></a>
     		<?php else : ?>
     			<a class="btn btn-warning" style="cursor: hand;"><?php echo __("U kunt geen segment toevoegen");?></a>
     		<?php endif; ?>
     		
        </div>
      	
      	<h1><?php echo $segment; ?></h1>
      	<p>
      		<?php echo __("Dit segment is ingevoerd op");?> <?php echo $result['Result']['created_dt']; ?>
      	</p>
      	
        <div class="well well-sm">
					
		        <!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
				  <li class="active"><a href="#chart" role="tab" data-toggle="tab"><?php echo __("Grafiek");?></a></li>
				  <li><a href="#table" role="tab" data-toggle="tab"><?php echo __("Tabel");?></a></li>
				  <li><a href="#scoretable" role="tab" data-toggle="tab"><?php echo __("Scoretabel");?></a></li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane" id="table">
				   <table class="table-striped table">
					<tr>
						<td>&nbsp;</td>
						<td><?php echo __("Rescaled gewicht");?></td>
						<td>&nbsp;</td>
						<td><span class='label label-default' style="background: rgba(124,181,236,1.0);"><?php echo $hotel_name; ?></span></td>
						<?php foreach($hotels as $k_hotel1=>$hotel) : ?>
						<td colspan="2"><span class='label label-default' style="background: <?php echo $this->App->GetHotelBackgroundColor($k_hotel1);?>;"><?php echo $hotel; ?></span></td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td><?php echo __("Gemiddelde kamerprijs");?></td>
						<td><?php $c122=($waardekenmerk_kamerprijs[0]/$waardekenmerk_kamerprijs[0]*100); echo $c122; ?>%</td>
						<td><?php echo $d122; ?></td>
						<td><?php $e122=($d122*$c122)/100; echo $e122; ?></td>
								<?php foreach($average_room_prices as $room_price) : ?>
						<td><?php echo $room_price; ?></td>
						<td><?php echo ($c122*$room_price)/100; ?></td>
						<?php endforeach; ?>
					</tr>
					<?php foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
						<?php $border_bottom= ($k==0) ? 'style="border-bottom: 1px solid #000;"' : ''; ?>
					<tr>
						<td <?= $border_bottom; ?>><?php echo $kenmerk; ?></td>
						<td <?= $border_bottom; ?>><?php echo $RescaledWeights[$k]; ?>%</td>
						
						<?php if($k==0) : ?>
							<td <?= $border_bottom; ?>>100</td>
							<td <?= $border_bottom; ?>><?php $e123= ($RescaledWeights[0]*100/100); echo $e123; ?></td>
							
							<?php for($p=0;$p <= ($hotel_amount-1); $p++) : ?>
								<td <?= $border_bottom; ?>><?php echo $segmenten[$p][$hotels[$p]][$kenmerk]; ?></td>
								<td <?= $border_bottom; ?>><?php echo number_format($e123*$segmenten[$p][$hotels[$p]][$kenmerk]/100,0); ?></td>
							<?php endfor; ?>
							
						<?php else : ?>
							<td <?= $border_bottom; ?>><?php echo $segmenten[$hotel_full_name][$kenmerk]; ?></td>
							<td <?= $border_bottom; ?>><?php $e124_32=($RescaledWeights[$k]*$kwaliteitskenmerken_uw_hotel[$k]/100); echo $e124_32; $e124_32_array[]=$e124_32; ?></td>
							
							<?php for($p=0;$p <= ($hotel_amount-1); $p++) : ?>
								<td <?= $border_bottom; ?>><?php echo $segmenten[$p][$hotels[$p]][$kenmerk]; ?></td>
								<td <?= $border_bottom; ?>><?php echo number_format($RescaledWeights[$k]*$segmenten[$p][$hotels[$p]][$kenmerk]/100,0); ?></td>
							<?php endfor; ?>
							
						<?php endif; ?>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><span class='label label-default' style="background: rgba(124,181,236,1.0);"><?php echo $hotel_name; ?></span></td>
						<?php foreach($hotels as $k_hotel2=>$hotel) : ?>
						<td colspan="2"><span class='label label-default' style="background: <?php echo $this->App->GetHotelBackgroundColor($k_hotel2);?>;"><?php echo $hotel; ?></span></td>
						<?php endforeach; ?>
					</tr>
						<tr>
						<td><?php echo __("GLOBAAL (Y-as)");?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php $e134= ($e122/10)-10; echo $e134; ?></td>
						<?php foreach($CoordsyAxis as $yAxis) : ?>
							<td colspan="2"><?php echo $yAxis; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td><?php echo __("GLOBAAL (X-as)");?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php $e135= ( (array_sum($e124_32_array)+$e123) /10)-10; echo $e135; ?></td>
						<?php foreach($CoordsxAxis as $xAxis) : ?>
							<td colspan="2"><?php echo $xAxis; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td><?php echo __("GLOBAAL (netto)");?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php echo $e135-$e134; ?></td>
						<?php foreach($this->App->CoordsNetto($CoordsxAxis,$CoordsyAxis) as $netto) : ?>
							<td colspan="2"><?php echo $netto; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<td><?php echo __("Aantal kamers");?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php echo $own_hotel_rooms; ?></td>
						<?php foreach($hotel_rooms as $hotel_room) : ?>
							<td colspan="2"><?php echo $hotel_room; ?></td>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<?php
						$coordsnetto_own_hotel=array((float)$e135-$e134);
						$coordsnetto=($this->App->CoordsNetto($CoordsxAxis,$CoordsyAxis)); 
						$coordsnetto=array_merge($coordsnetto_own_hotel,$coordsnetto);						
						$ranking=$this->App->GetRanking($coordsnetto);						
						?>
						<td><?php echo __("Rangorde");?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<?php foreach ($coordsnetto as $val) : ?>
							<td colspan="2" align="center"><?php $newVal = (string) $val;
							echo $ranking[$newVal]; ?></td>
						<?php endforeach; ?>
					</tr>
				</table>
				  </div>
				  <div class="tab-pane active" id="chart">
					<div id="container-chart" style="height: 600px; min-width: 510px; width: 1020px; max-width: 1020px; margin: 0 auto"></div>
					
					<?php /*<div id="controls">		
					<button class="btn-primary btn" style="margin-top: 10px;" id="reset"><?php echo __("Reset diagram");?></button>
					
					<table class="table-striped table" style="margin-top: 10px;">
					<tr>
						<td>&nbsp;</td>
						<td><?php echo __("X-as");?></td>
						<td><?php echo __("Y-as");?></td>
						<td><?php echo __("Kamers");?></td>
					</tr>
					<tr>
						<td><span class='label label-default' style="background: rgba(124,181,236,1.0);"><?php echo $hotel_full_name; ?></span></td>
						<td><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $e135; ?>" id="hotelown_x" /></td>
						<td><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $e134; ?>" id="hotelown_y" /></td>
						<td><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $own_hotel_rooms; ?>" id="hotelown_rooms" /></td>
					</tr>
					<?php foreach($hotels as $k=>$hotel) : ?>
					<tr>
						<td width="100"><span class='label label-default' style="background: <?php echo $this->App->GetHotelBackgroundColor($k);?>;"><?php echo $hotel; ?></span></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $CoordsxAxis[$k]; ?>" id="hotel<?php echo $k; ?>_x" /></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="-100" data-displayPrevious=true value="<?php echo $CoordsyAxis[$k]; ?>" data-fgColor="#66CC66" id="hotel<?php echo $k; ?>_y"  /></td>
						<td width="40"><input class="knob knop" data-width="80" data-height="80" data-min="0" data-max="300" data-displayPrevious=true value="<?php echo $hotel_rooms[$k]; ?>" data-fgColor="#ffec03" id="hotel<?php echo $k; ?>_rooms" /></td>
					</tr>
					<?php endforeach; ?>
					</table>
					
					
					
					<form method="GET" id="calc">
					<table class="table-striped table" style="margin-top: 10px;">
					<tr>
						<td>&nbsp;</td>
						<td><?php echo __("Kamerprijs");?></td>
						<?php foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
							<td><?php echo $kenmerk;?></td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td><span class='label label-default' style="background: rgba(124,181,236,1.0);"><?php echo $hotel_full_name; ?></span></td>
						<td><input class="knob knop" data-width="60" data-height="60" data-min="1" data-max="300" data-displayPrevious=true value="<?php echo $kamerprijs_uw_hotel[0]; ?>" id="hotelownhotel_kamerprijs" name="hotelownhotel_kamerprijs" /></td>
						<td><input class="knob knop" data-width="60" data-height="60" data-min="1" data-max="10" data-displayPrevious=true value="8.5" id="hotelownhotel_reviewscore" name="hotelownhotel_reviewscore" /></td>
						<td colspan="9"></td>
					</tr>
					<?php foreach($hotels as $k_hotel10=>$hotel) : ?>
						<tr>
						<td><span class='label label-default' style="background: <?php echo $this->App->GetHotelBackgroundColor($k_hotel10);?>;"><?php echo $hotel; ?></span></td>
						<td><input class="knob knop" data-width="60" data-height="60" data-min="1" data-max="500" data-displayPrevious=true value="<?php echo $kamerprijs_gemiddeld[$k_hotel10]; ?>" id="hotel<?php echo $k_hotel10; ?>_kamerprijs" name="hotel<?php echo $k_hotel10; ?>_kamerprijs" /></td>
						<td><input class="knob knop" data-width="60" data-height="60" data-min="1" data-max="10" data-displayPrevious=true value="<?php echo $review_scores[$k_hotel10]; ?>" id="hotel<?php echo $k_hotel10; ?>_reviewscore" /></td>
						<?php unset($kwaliteitskenmerken[0]); ?>
						<?php foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
							<td>
								<?php unset($segmenten[$k_hotel10][$hotel]['Review score']); ?>
								<?php $benchmark_hotel=($segmenten[$k_hotel10][$hotel][$kenmerk]-$kwaliteitskenmerken_uw_hotel[$k]) / 100 * 100; ?>
								<input class="knob knop" data-width="60" data-height="60" data-min="-300" data-max="300" data-displayPrevious=true value="<?php echo $benchmark_hotel; ?>" id="hotel<?php echo $k_hotel10; ?>_<?php echo $k; ?>" /></td>
						<?php endforeach; ?>
					</tr>
					<?php endforeach; ?>
					
					</table>
					</form>
					
					</div>*/ ?>
					
				  </div>
				  <div class="tab-pane" id="scoretable">
					  <table class="table-striped table">
						<tr>
							<td>&nbsp;</td>
							<td><span class='label label-default' style="background: rgba(124,181,236,1.0);"><?php echo $hotel_name; ?></span></td>
							<?php foreach($hotels as $k_hotel3=>$hotel) : ?>
							<td colspan="2"><span class='label label-default' style="background: <?php echo $this->App->GetHotelBackgroundColor($k_hotel3);?>;"><?php echo $hotel; ?></span></td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td><?php echo __("Marktpositie t.a.v. kamerprijs");?></td>
							<td><?php echo __("= benchmark");?></td>
									<?php foreach($average_room_prices as $room_price) : ?>
							<td><?php $benchmark_room_price= abs( ($room_price - $d122) / 100 * 100); echo $benchmark_room_price; ?>%</td>
							<td><?php echo $this->App->GetBeterOfSlechter($benchmark_room_price); ?></td>
							<?php endforeach; ?>
						</tr>
						<?php 
						foreach($kwaliteitskenmerken as $k=>$kenmerk) : ?>
							<?php $border_bottom= ($k==0) ? 'style="border-bottom: 1px solid #000;"' : ''; ?>
						<tr>
							<td <?= $border_bottom; ?>><?php echo __("Marktpositie t.a.v.");?> <?php echo $kenmerk; ?></td>
							<td <?= $border_bottom; ?>><?php echo __("= benchmark");?></td>
			
							<?php for($p=0;$p <= ($hotel_amount-1); $p++) : ?>
								<td <?= $border_bottom; ?>><?php $benchmark_hotel=($segmenten[$p][$hotels[$p]][$kenmerk]-$kwaliteitskenmerken_uw_hotel[$p+1]) / 100 * 100; echo $benchmark_hotel; ?>%</td>
								<td <?= $border_bottom; ?>><?php echo $this->App->GetBeterOfSlechter($benchmark_hotel); ?></td>
							<?php endfor; ?>
							
						</tr>
						<?php endforeach; ?>
						<tr>
							<td><?php echo __("Rangorde");?></td>
						<?php foreach ($coordsnetto as $val) : ?>
							<td colspan="2" align="center"><?php $newVal = (string) $val;
							echo $ranking[$newVal]; ?></td>
						<?php endforeach; ?>
						</tr>
					</table>
				  </div>
				</div>
								
                </div>
                <?php echo $text_blocks[1]['Text']['tekst']; ?>
              </div>
				 </div>
   
          </div>  
		</div>
</div>

 <script>
				$(document).ready(function() {
					
					$('#reset').click(function(){
						location.reload();
					});
					
					// Update HighCharts with the jQuery Knob
					$(".knob").knob({
					    change: function (value) {        
					        // Retrieve all input from form
					       
					        $.get( "/results/dynamicchart/<?php echo $id; ?>/", $('#calc').serialize(), function( data ) {

							  var CoordsxAxis = data;
							  
							  
	
									  	var hotelown_x = 0;//$("#hotelown_x").val();
										var hotelown_y = 0;//$("#hotelown_y").val();
										var hotelown_rooms = 0;//$("#hotelown_rooms").val();
										
							        <?php if(isset($hotels[0])) : ?>
										var hotel0_x = CoordsxAxis[0];
										var hotel0_y = 0;//CoordsyAxis[0];
										var hotel0_rooms = 0;//$("#hotel0_rooms").val();
									<?php else : ?>
										var hotel0_x = 0;
										var hotel0_y = 0;
										var hotel0_rooms = 0;
									<?php endif; ?>
									
									<?php if(isset($hotels[1])) : ?>
										var hotel1_x = CoordsxAxis[1];
										var hotel1_y = 0;//CoordsyAxis[1];
										var hotel1_rooms = 0;//$("#hotel1_rooms").val();
									<?php else : ?>
										var hotel1_x = 0;
										var hotel1_y = 0;
										var hotel1_rooms = 0;
									<?php endif; ?>
									
									<?php if(isset($hotels[2])) : ?>
										var hotel2_x = CoordsxAxis[2];
										var hotel2_y = 0;//CoordsyAxis[2];
										var hotel2_rooms = 0;//$("#hotel2_rooms").val();
									<?php else : ?>
										var hotel2_x = 0;
										var hotel2_y = 0;
										var hotel2_rooms = 0;
									<?php endif; ?>
									
									
									<?php if(isset($hotels[3])) : ?>
										var hotel3_x = CoordsxAxis[3];
										var hotel3_y = 0;//CoordsyAxis[3];
										var hotel3_rooms = 0;//$("#hotel3_rooms").val();
									<?php else : ?>
										var hotel3_x = 0;
										var hotel3_y = 0;
										var hotel3_rooms = 0;
									<?php endif; ?>
									
									<?php if(isset($hotels[4])) : ?>
										var hotel4_x = CoordsxAxis[4];
										var hotel4_y = 0;//CoordsyAxis[4];
										var hotel4_rooms = 0;//$("#hotel4_rooms").val();
									<?php else : ?>
										var hotel4_x = 0;
										var hotel4_y = 0;
										var hotel4_rooms = 0;
									<?php endif; ?>
				
							        $('#container-chart').highcharts({
							
									    chart: {
									        type: 'bubble',
									        zoomType: 'xy',
									        animation: false
									    },
								
									    title: {
									    	text: '<?php echo __("Marktpositie Analyse Tool");?>'
									    },
								        
								      yAxis: {
											min: (Math.min(hotelown_y, hotel0_y, hotel1_y, hotel2_y, hotel3_y, hotel4_y) - 20),
									    	max: (Math.max(hotelown_y, hotel0_y, hotel1_y, hotel2_y, hotel3_y, hotel4_y) + 20),
									    		    	lineWidth:1,
								        gridLineWidth: 0,
								            title: {
								                text:  '<?php echo __("Mate waarin een hotel Duurder (+) or Goedkoper (-) is (y-as)");?>'  
								            },
								                plotLines: [{
								            color: '#000',
								            width: 1,
								            value: 0
								        }] 
								    },
								
									    xAxis: {
									 		min: (Math.min(hotelown_x, hotel0_x, hotel1_x, hotel2_x, hotel3_x, hotel4_x) - 20),
									    	max: (Math.max(hotelown_x, hotel0_x, hotel1_x, hotel2_x, hotel3_x, hotel4_x) + 20),
									    	lineWidth:1,
								        gridLineWidth: 0,
								        
								            title: {
								                text:  '<?php echo __("Mate waarin een hotel meer (+) of minder (-)  klantwaarde biedt (x-as)");?>'  
								            },
								        plotLines: [{
								            color: '#000',
								            width: 1,
								            value: 0
								        }]    
								    },
									    series: [
													{
												    	name: '<?php echo $hotel_name; ?>',
												        data: [[parseFloat(hotelown_x),parseFloat(hotelown_y),parseFloat(hotelown_rooms)]]
												    }, 
												    <?php if(isset($hotels[0])) : ?>
									    			{
												    	name: '<?php echo $hotels[0]; ?>',
												        data: [[parseFloat(hotel0_x),parseFloat(hotel0_y),parseFloat(hotel0_rooms)]]
												    }, 
												    <?php endif; ?>
												    <?php if(isset($hotels[1])) : ?>
												    {
												    	name: '<?php echo $hotels[1]; ?>',
												        data: [[parseFloat(hotel1_x),parseFloat(hotel1_y),parseFloat(hotel1_rooms)]]
												    },
												    <?php endif; ?>
												    <?php if(isset($hotels[2])) : ?> 
												    {
												    	name: '<?php echo $hotels[2]; ?>',
												        data: [[parseFloat(hotel2_x),parseFloat(hotel2_y),parseFloat(hotel2_rooms)]]
											        },
											        <?php endif; ?>
											        <?php if(isset($hotels[3])) : ?>
											        {
											        	name: '<?php echo $hotels[3]; ?>',
											        	data: [[parseFloat(hotel3_x),parseFloat(hotel3_y),parseFloat(hotel3_rooms)]]
											        },
											        <?php endif; ?>
											        <?php if(isset($hotels[4])) : ?>
											        {
											        	name: '<?php echo $hotels[4]; ?>',
											        	data: [[parseFloat(hotel4_x),parseFloat(hotel4_y),parseFloat(hotel4_rooms)]]
											        },
											        <?php endif; ?>
											        							        {
									            type: 'line',
									            name: '<?php echo __("Indifferentielijn y = x");?>',
									            data: [[-25, -25], [20, 20]],
									            marker: {
									                enabled: false
									            },
									            states: {
									                hover: {
									                    lineWidth: 0
									                }
									            },
									            enableMouseTracking: false
									        }
												]
									
									},
								function (chart) { // on complete
								        chart.renderer.text('<?php echo __("Competitief nadeel (verlies marktaandeel)");?>', 120, 50)
								            .css({
								                color: '#000',
								                fontSize: '13px'
								            })
										.add();
										
										chart.renderer.text('<?php echo __("Competitief voordeel (toenemend marktaandeel)");?>', 620, 480)
								            .css({
								                color: '#000',
								                fontSize: '13px'
								            })
										.add();
								    });
									  
							  
							  
							  
							  
							  
							  
							}, "json");
					        
							
					    }
					});
				
				
					// Load Diagram init
				     $('#container-chart').highcharts({
					    chart: {
					        type: 'bubble',
					        zoomType: 'xy'
					    },
				
					    title: {
					    	text: '<?php echo __("Marktpositie Analyse Tool");?>'
					    },
				        
				        	    yAxis: {
							min: <?php echo ( min($CoordsyAxis) - 20 ); ?>,
						    max: <?php echo ( max($CoordsyAxis) + 20 ); ?>,
					    		    	lineWidth:1,
				        gridLineWidth: 0,
				            title: {
				                text:  '<?php echo __("Mate waarin een hotel Duurder (+) or Goedkoper (-) is (y-as)");?>'  
				            },
				                plotLines: [{
				            color: '#000',
				            width: 1,
				            value: 0
				        }] 
				    },
				
					    xAxis: {
					    	min: <?php echo ( min($CoordsxAxis) - 20 ); ?>,
						    max: <?php echo ( max($CoordsxAxis) + 20 ); ?>,
					    	lineWidth:1,
				        gridLineWidth: 0,
				        
				            title: {
				                text:  '<?php echo __("Mate waarin een hotel meer (+) of minder (-)  klantwaarde biedt (x-as)");?>'  
				            },
				        plotLines: [{
				            color: '#000',
				            width: 1,
				            value: 0
				        }]    
				    },
					    series: [
									{
								    	name: '<?php echo $hotel_name; ?>',
								        data: [[ <?php echo $e134; ?>,<?php echo $e135; ?>,<?php echo $own_hotel_rooms; ?>]]
								    },
								    <?php if(isset($hotels[0])) : ?> 
					    			{
								    	name: '<?php echo $hotels[0]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[0],$CoordsyAxis[0],$hotel_rooms[0] ); ?>]]
								    },
								    <?php endif; ?>
								    <?php if(isset($hotels[1])) : ?> 
								    {
								    	name: '<?php echo $hotels[1]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[1],$CoordsyAxis[1],$hotel_rooms[1] ); ?>]]
								    },
								    <?php endif; ?> 
								    <?php if(isset($hotels[2])) : ?>
								    {
								    	name: '<?php echo $hotels[2]; ?>',
								        data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[2],$CoordsyAxis[2],$hotel_rooms[2] ); ?>]]
							        },
							        <?php endif; ?>
							        <?php if(isset($hotels[3])) : ?>
							        {
							        	name: '<?php echo $hotels[3]; ?>',
							        	data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[3],$CoordsyAxis[3],$hotel_rooms[3] ); ?>]]
							        },
							        <?php endif; ?>
							        <?php if(isset($hotels[4])) : ?>
							        {
							        	name: '<?php echo $hotels[4]; ?>',
							        	data: [[<?php echo $this->App->GenerateSeriesData( $CoordsxAxis[4],$CoordsyAxis[4],$hotel_rooms[4] ); ?>]]
							        },
							        <?php endif; ?>
							        {
							            type: 'line',
							            name: '<?php echo __("Indifferentielijn y = x");?>',
							            data: [[-25, -25], [20, 20]],
							            marker: {
							                enabled: false
							            },
							            states: {
							                hover: {
							                    lineWidth: 0
							                }
							            },
							            enableMouseTracking: false
							        }
								]
					
					},
					function (chart) { // on complete
					        chart.renderer.text('<?php echo __("Competitief nadeel (verlies marktaandeel)");?>', 120, 50)
					            .css({
					                color: '#000',
					                fontSize: '13px'
					            })
							.add();
							
							chart.renderer.text('<?php echo __("Competitief voordeel (toenemend marktaandeel)");?>', 620, 480)
					            .css({
					                color: '#000',
					                fontSize: '13px'
					            })
							.add();
					    });
				});
				
				// Default Draw function
				$(function() {
					$(".knob").knob({
					    draw : function () {
					
					        // "tron" case
					        if(this.$.data('skin') == 'tron') {
					
					            var a = this.angle(this.cv)  // Angle
					                , sa = this.startAngle          // Previous start angle
					                , sat = this.startAngle         // Start angle
					                , ea                            // Previous end angle
					                , eat = sat + a                 // End angle
					                , r = true;
					
					            this.g.lineWidth = this.lineWidth;
					
					            this.o.cursor
					                && (sat = eat - 0.3)
					                && (eat = eat + 0.3);
					
					            if (this.o.displayPrevious) {
					                ea = this.startAngle + this.angle(this.value);
					                this.o.cursor
					                    && (sa = ea - 0.3)
					                    && (ea = ea + 0.3);
					                this.g.beginPath();
					                this.g.strokeStyle = this.previousColor;
					                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
					                this.g.stroke();
					            }
					
					            this.g.beginPath();
					            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
					            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
					            this.g.stroke();
					
					            this.g.lineWidth = 2;
					            this.g.beginPath();
					            this.g.strokeStyle = this.o.fgColor;
					            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
					            this.g.stroke();
					
					            return false;
					        }
					    }
					});
				});
				</script>