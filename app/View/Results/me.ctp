<div class="container">
	<div class="row">
      <div class="col-xs-12 col-sm-6 col-lg-11">
		<h1><?php echo __("Mijn marktpositie analyses");?></h1>
		<p>
			<?php echo __("Dit is het overzicht van de door u ingevoerde marktpositie analyses. De laatst ingevoerde analyse staat bovenaan. U ziet de segmenten die door u zijn ingevoerd en u ziet de sessie. Wanneer u ziet dat de sessies gelijk zijn, dan betekent dit dat deze segmenten bij elkaar horen.");?>
		</p>
		
		<p>
			<?php if($sessionId) : ?>			
				<?php if(!$segments) : ?>
					<a href="/segments/choose/" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> <?php echo __("Segment toevoegen");?></a>
				<? elseif( count($segments) == 1 || count($segments) == 2 ) : ?>
	     			<a href="/segments/restart/" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> <?php echo __("Segment toevoegen");?></a>
	     		<?php else : ?>
	     			<a class="btn btn-warning" style="cursor: hand;"><?php echo __("U kunt geen segment toevoegen");?></a>
	     		<?php endif; ?>
	     		<?php else : ?>
	     			<a href="/segments/choose/" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> <?php echo __("Segment toevoegen");?></a>
			<?php endif; ?>
		</p>
          <table class="table table-striped table-condensed">
                  <thead>
                  <tr>
                      <th><?php echo __("Datum");?></th>
                      <th><?php echo __("Segment");?></th>
                      <th><?php echo __("Sessie");?></th>                                                           
                  </tr>
              </thead>   
              <tbody>
              	<?php foreach ($results as $result) : ?>
                <tr>
                    <td><?php echo $result['Result']['created_dt']; ?></td>                    
                    <td><?php $session_data=unserialize($result['Result']['session_data']); ?>
					<a href="/results/view/<?php echo $result['Result']['id'];?>/"><?php echo $session_data[0]['Page']['segment1'];?></a></td>
					<td><?php echo $result['Result']['session_id']; ?></td>                                       
                </tr>   
                <?php endforeach; ?>                              
              </tbody>
            </table>
		</div>
	</div>
</div>