<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
			<?php echo $this->Session->flash(); ?>
	        <div class="page-header">
	            <h2><?php echo __('Resultaten'); ?></h2>
	        </div>
			
			<table class="table table-bordered table-striped">
            <thead>
                <tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('username','Hotel'); ?></th>
					<th><?php echo $this->Paginator->sort('segment','Segment'); ?></th>
					<th><?php echo $this->Paginator->sort('session_id','SessionId'); ?></th>
					<th><?php echo $this->Paginator->sort('created_dt','Aangemaakt op'); ?></th>
					<th>Acties</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $result): ?>
                    <tr>
						<td><?php echo $result['Result']['id']; ?></td>
						<td><?php echo $result['UserJoin']['username']; ?></td>
						<td>
							<?php $session_data= unserialize($result['Result']['session_data']);
							echo $session_data['Page']['segment1'];
							 ?>
						</td>
						<td><?php echo $result['Result']['session_id']; ?></td>
						<td><?php echo $result['Result']['created_dt']; ?></td>
                        <td>
                        <?php echo $this->Html->link(__d('cake', 'Bekijken'), array('admin'=>false,'controller' => 'results', 'action' => 'view', $result['Result']['id']), array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="well">
            <?php echo $this->Paginator->counter(array('format' => __d('cake', 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
        </div>
        <?php if ($this->Paginator->numbers()): ?>
            <div class="pagination">
                <ul>
                    <?php echo $this->Paginator->first(); ?>
                    <?php echo $this->Paginator->prev(); ?>
                    <?php echo $this->Paginator->numbers(); ?>
                    <?php echo $this->Paginator->next(); ?>
                    <?php echo $this->Paginator->last(); ?>
                </ul>
            </div>
        <?php endif; ?>
      	</div>
	</div>      	
</div>