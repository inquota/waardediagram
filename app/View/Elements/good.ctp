<div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <?php echo h($message); ?>
</div>