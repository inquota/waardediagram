<nav style="margin-bottom: 0;" role="navigation" class="navbar navbar-default">
  <div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button data-target=".navbar-bootsnipp-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
      <span class="sr-only"><?php echo __("Toon navigatie");?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-bootsnipp-collapse">
    <ul class="nav navbar-nav">
     	<li class="active"><a href="/admin/pages/dashboard/"><span class="glyphicon glyphicon-home"></span> <?php echo __("Home");?></a></li>
     	<li class=""><a href="/admin/texts/"><?php echo __("Tekst blokken");?></a></li>
     	<li class=""><a href="/admin/app_users/"><?php echo __("Gebruikers");?></a></li>
     	<li class=""><a href="/admin/qualities/"><?php echo __("Kwaliteitskenmerken");?></a></li>
     	<li class=""><a href="/admin/results/"><?php echo __("Resultaten");?></a></li>
     </ul>

    <ul class="nav navbar-nav navbar-right">    	
       <li class="" id="nav-login-btn"><a href="/users/logout/"><i class="icon-login"></i><?php echo __("Uitloggen");?></a></li>
    </ul>
  </div><!-- /.navbar-collapse -->
  </div>
</nav>