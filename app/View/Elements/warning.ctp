<div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>Waarschuwing</strong> <?php echo h($message); ?>
</div>