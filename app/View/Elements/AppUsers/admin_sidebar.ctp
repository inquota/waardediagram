<div class="actions">
	<ul>
		<li>
			<?php echo $this->Html->link(__d('users', 'Logout'), array('admin' => false, 'plugin' => null, 'controller' => 'app_users', 'action' => 'logout')); ?>
		</li>
		<li>
			<?php echo $this->Html->link(__d('users', 'My Account'), array('admin' => false, 'plugin' => null,  'controller' => 'app_users', 'action' => 'edit')); ?>
		</li>
		<li>&nbsp;</li>
		<li>
			<?php echo $this->Html->link(__d('users', 'Add Users'), array('admin' => true, 'plugin' => null, 'controller' => 'app_users', 'action'=>'add'));?>
		</li>
		<li>
			<?php echo $this->Html->link(__d('users', 'List Users'), array('admin' => true, 'plugin' => null, 'controller' => 'app_users', 'action'=>'index'));?>
		</li>
		<li>&nbsp;</li>
		<li>
			<?php echo $this->Html->link(__d('users', 'Frontend'), array('admin' => false, 'plugin' => null, 'controller' => 'app_users', 'action'=>'index')); ?>
		</li>
	</ul>
</div>