<?php
$menu=array(
			'menu1'=>array(__('Hoe werkt het?')=>'/hoe-werkt-het/'),
			'menu2'=>array(__('Registreren')=>'/register/', __('Inloggen')=>'/login/'),
			'menu3'=>array(__('Mijn analyses')=>'/results/me/')
			);
?>
<nav style="margin-bottom: 0;" role="navigation" class="navbar navbar-default">
  <img src="/img/hotelschool-logo.png" alt="Logo Hotelschool" class="pull-left" />
	
  <div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button data-target=".navbar-bootsnipp-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
      <span class="sr-only"><?php echo __("Toon navigatie");?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-bootsnipp-collapse">
    <ul class="nav navbar-nav">
    	<?php if($this->params->here == '/') : ?>
    		<li class="active"><a href="/"><span class="glyphicon glyphicon-home"></span> <?php echo __("Home");?></a></li>
    	<?php else : ?>
    		<li><a href="/"><span class="glyphicon glyphicon-home"></span> <?php echo __("Home");?></a></li>
    	<?php endif; ?>	
    	
    	<?php foreach($menu['menu1'] as $k=>$item): ?>
    		<?php if($this->params->here == $item) : ?>
    				<li class="active"><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php else : ?>
    				<li class=""><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php endif; ?>
    	<?php endforeach; ?>          
	</ul>

    <ul class="nav navbar-nav navbar-right">    	
    	<?php if($is_logged_in==null) : ?>
		   <?php foreach($menu['menu2'] as $k=>$item): ?>
    		<?php if($this->params->here == $item) : ?>
    				<li class="active"><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php else : ?>
    				<li class=""><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php endif; ?>
    	<?php endforeach; ?>  
       <?php else : ?>
       	   <?php foreach($menu['menu3'] as $k=>$item): ?>
    		<?php if($this->params->here == $item) : ?>
    				<li class="active"><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php else : ?>
    				<li class=""><a href="<?php echo $item; ?>"><i class="icon-login"></i><?php echo $k;?></a></li>
    		<?php endif; ?>
    	<?php endforeach; ?>  
			
		<?php if(strstr($this->params->here, '/users/')) : ?>
				<li class="dropdown active">
			      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Mijn profiel'); ?> <b class="caret"></b></a>
			      <ul class="dropdown-menu">
			        <li class=""><a href="/users/edit/"> <?php echo __('Bewerken'); ?></a></li>
			        <li class=""><a href="/users/change_password/"><?php echo __('Wachtwoord aanpassen'); ?></a></li>
					</ul>
			      </li>
		<?php else : ?>
				<li class="dropdown">
			      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Mijn profiel'); ?> <b class="caret"></b></a>
			      <ul class="dropdown-menu">
			        <li class=""><a href="/users/edit/"> <?php echo __('Bewerken'); ?></a></li>
			        <li class=""><a href="/users/change_password/"><?php echo __('Wachtwoord aanpassen'); ?></a></li>
					</ul>
			      </li>
		<?php endif; ?>
	
		<?php if($this->params->here == '/users/logout/') : ?>
				<li class="active"><a href="/users/"><i class="icon-login"></i><?php echo __('Uitloggen'); ?></a></li>
		<?php else : ?>
				<li class=""><a href="/users/logout/"><i class="icon-login"></i><?php echo __('Uitloggen'); ?></a></li>
		<?php endif; ?>

       	<?php endif; ?>
       	<li style="margin-left: 20px;">
			<div class="btn-group" style="margin-top: 25px;">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			    Taal/Language <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li><a href="/pages/language/eng/"><span class="flag-icon flag-icon-gb"></span> English</a></li>
			    <li><a href="/pages/language/nld/"><span class="flag-icon flag-icon-nl"></span> Nederlands</a></li>			    
			  </ul>
			</div>
		</li>
         </ul>
  </div><!-- /.navbar-collapse -->
  
  </div>
  <img src="/img/sia-logo.gif" alt="Logo SIA" id="logo_sia" class="pull-right" style="margin-top: -90px;" />
</nav>