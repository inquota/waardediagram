<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
        	<?php echo $this->Session->flash(); ?>
        	<?php echo $this->Form->create('Text', array('class'=>'form-horizontal'));?>
				<div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Tekst</label>
                   <div class="col-sm-9 col-lg-10 controls">
                      <?php echo $this->Form->input('tekst', array('type'=>'textarea', 'cols'=>20,'rows'=>20,'label'=>false,'class'=>'ckeditor form-control')); ?>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Variabelen</label>
                   <div class="col-sm-9 col-lg-10 controls">
                      <?php echo $this->Form->input('page_vars', array('label'=>false,'class'=>'form-control')); ?>
                      Dit zijn variabelen die in de tekst gebruikt kunnen worden specifiek voor deze pagina. U kunt zelf geen variabelen aanmaken, dan ontstaat er een script fout.
                   </div>
                </div>
				<div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Taal</label>
                   <div class="col-sm-9 col-lg-10 controls">
                      <select name="data[Text][language]">
                      	<option value="<?php echo $this->data['Text']['language']; ?>"><?php echo $this->data['Text']['language']; ?></option>
                      	<option value="NL">Nederlands</option>
                      	<option value="EN">Engels</option>
                      </select>
                   </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                       <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Opslaan</button>
                    </div>
                </div>
      			<?php echo $this->Form->end();?>
     	</div>
	</div>      	
</div>