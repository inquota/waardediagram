<div class="row" style="width: 1020px !important;">
	<div class="col-md-12">
	    <div class="box">
	        <div class="box-title">
	            <h3><i class="icon-reorder"></i>Tekst element toevoegen</h3>
	            <div class="box-tool">
	                <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
	                <a data-action="close" href="#"><i class="icon-remove"></i></a>
	            </div>
	        </div>
	        <div class="box-content">
	        	<?php echo $this->Session->flash(); ?>
	        	<?php echo $this->Form->create('Text', array('class'=>'form-horizontal'));?>
					<div class="form-group">
	                   <label class="col-sm-3 col-lg-2 control-label">Sectie</label>
	                   <div class="col-sm-9 col-lg-10 controls">
	                      <?php echo $this->Form->input('section', array('label'=>false,'class'=>'form-control','required'=>'required')); ?>
	                   </div>
	                </div>
					<div class="form-group">
	                   <label class="col-sm-3 col-lg-2 control-label">Label</label>
	                   <div class="col-sm-9 col-lg-10 controls">
	                      <?php echo $this->Form->input('label', array('label'=>false,'class'=>'form-control','required'=>'required')); ?>
	                   </div>
	                </div>
					<div class="form-group">
	                   <label class="col-sm-3 col-lg-2 control-label">Tekst</label>
	                   <div class="col-sm-9 col-lg-10 controls">
	                      <?php echo $this->Form->input('tekst', array('type'=>'textarea','label'=>false,'class'=>'ckeditor form-control','required'=>'required')); ?>
	                   </div>
	                </div>
					<div class="form-group">
	                   <label class="col-sm-3 col-lg-2 control-label">Variabelen</label>
	                   <div class="col-sm-9 col-lg-10 controls">
	                      <?php echo $this->Form->input('page_vars', array('label'=>false,'class'=>'form-control')); ?>
	                      Dit zijn variabelen die in de tekst gebruikt kunnen worden specifiek voor deze pagina. U kunt zelf geen variabelen aanmaken, dan ontstaat er een script fout.
	                   </div>
	                </div>
					<div class="form-group">
	                   <label class="col-sm-3 col-lg-2 control-label">Taal</label>
	                   <div class="col-sm-9 col-lg-10 controls">
	                      <select name="data[Text][language]">
	                      	<option value="NL">Nederlands</option>
	                      	<option value="EN">Engels</option>
	                      </select>
	                   </div>
	                </div>
	                <div class="form-group">
	                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
	                       <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Opslaan</button>
	                    </div>
	                </div>
	      			<?php echo $this->Form->end();?>
	        </div>
	    </div>
	</div>
</div>