<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
			<?php echo $this->Session->flash(); ?>
	        <div class="page-header">
	            <h2><?php echo __('Tekst blokken'); ?></h2>
	        </div>

	        <table class="table table-bordered table-striped">
	            <thead>
	                <tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('section'); ?></th>
						<th><?php echo $this->Paginator->sort('label'); ?></th>
						<th><?php echo $this->Paginator->sort('tekst'); ?></th>
						<th><?php echo $this->Paginator->sort('language'); ?></th>
	                    <th colspan="3"><?php echo __d('cake', 'Acties'); ?></th>
	                </tr>
	            </thead>
	            <tbody>
	                <?php foreach ($users as $customer): ?>
	                    <tr>
							<td><?php echo $customer['Text']['id']; ?></td>
							<td><?php echo $customer['Text']['section']; ?></td>
							<td><?php echo $customer['Text']['label']; ?></td>
							<td><?php echo htmlentities($this->App->Substract($customer['Text']['tekst'],200)); ?></td>
							<td><?php echo $customer['Text']['language']; ?></td>
	                        <td>
	                        <?php echo $this->Html->link(__d('cake', 'Bewerken'), array('controller' => 'texts', 'action' => 'edit', $customer['Text']['id']), array('class' => 'btn btn-warning')); ?>
	                        </td>
	                    </tr>
	                <?php endforeach; ?>
	            </tbody>
	        </table>
	        <div class="well">
	            <?php echo $this->Paginator->counter(array('format' => __d('cake', 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
	        </div>
	        <?php if ($this->Paginator->numbers()): ?>
	            <div class="pagination">
	                <ul>
	                    <?php echo $this->Paginator->first(); ?>
	                    <?php echo $this->Paginator->prev(); ?>
	                    <?php echo $this->Paginator->numbers(); ?>
	                    <?php echo $this->Paginator->next(); ?>
	                    <?php echo $this->Paginator->last(); ?>
	                </ul>
	            </div>
	        <?php endif; ?>
      	</div>
	</div>      	
</div>