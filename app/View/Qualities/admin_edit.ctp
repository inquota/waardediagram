<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
			<?php echo $this->Session->flash(); ?>
	        <div class="page-header">
	            <h2><?php echo __('Tekst blokken'); ?></h2>
	        </div>
        	<?php echo $this->Session->flash(); ?>
        	<?php echo $this->Form->create('Quality', array('class'=>'form-horizontal'));?>
				<div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Titel</label>
                   <div class="col-sm-9 col-lg-10 controls">
                      <?php echo $this->Form->input('title', array('label'=>false,'class'=>'form-control')); ?>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Volgorde</label>
                   <div class="col-sm-9 col-lg-10 controls">
                      <?php echo $this->Form->input('sequence', array('label'=>false,'class'=>'form-control')); ?>
                   </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                       <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Opslaan</button>
                    </div>
                </div>
      			<?php echo $this->Form->end();?>
     	</div>
	</div>      	
</div>