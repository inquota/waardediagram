<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
			<?php echo $this->Session->flash(); ?>
	        <div class="page-header">
	            <h2><?php echo __('Kwaliteitskenmerken'); ?></h2>
	        </div>

        <table class="table table-bordered table-striped">
        <!-- <table class="datagrid"> -->
            <thead>
                <tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('title', 'Titel'); ?></th>
					<th><?php echo $this->Paginator->sort('sequence', 'Volgorde'); ?></th>
                    <th colspan="3"><?php echo __d('cake', 'Acties'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($qualities as $quality): ?>
                    <tr>
						<td><?php echo $quality['Quality']['id']; ?></td>
						<td><?php echo $quality['Quality']['title']; ?></td>
						<td><?php echo $quality['Quality']['sequence']; ?></td>
                        <td>
                        <?php echo $this->Html->link(__d('cake', 'Bewerken'), array('controller' => 'qualities', 'action' => 'edit', $quality['Quality']['id']), array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="well">
            <?php echo $this->Paginator->counter(array('format' => __d('cake', 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
        </div>
        <?php if ($this->Paginator->numbers()): ?>
            <div class="pagination">
                <ul>
                    <?php echo $this->Paginator->first(); ?>
                    <?php echo $this->Paginator->prev(); ?>
                    <?php echo $this->Paginator->numbers(); ?>
                    <?php echo $this->Paginator->next(); ?>
                    <?php echo $this->Paginator->last(); ?>
                </ul>
            </div>
        <?php endif; ?>
      	</div>
	</div>      	
</div>