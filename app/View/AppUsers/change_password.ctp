<div class="container">
	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<?php echo $this->Form->create($model, array('action' => 'change_password','role'=>'form') ); ?>
				<h2><?php echo __d('users', 'Change your password'); ?></h2>
				<p><?php echo __d('users', 'Please enter your old password because of security reasons and then your new password twice.'); ?></p>
				<hr class="colorgraph">
				<div class="form-group">		
					<label><?php echo __("Oude wachtwoord");?></label>		
					<?php echo $this->Form->input('old_password', array('type'=>'password','label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Oude wachtwoord'), 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">		
					<label><?php echo __("Nieuwe wachtwoord");?></label>		
					<?php echo $this->Form->input('new_password', array('type'=>'password','label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Nieuw wachtwoord'), 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">		
					<label><?php echo __("Bevestig wachtwoord");?></label>		
					<?php echo $this->Form->input('confirm_password', array('type'=>'password','label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Bevestig wachtwoord'), 'tabindex'=>'3')); ?>
				</div>
			
				<hr class="colorgraph">
				<div class="row" style="padding-bottom: 20px;">
					<div class="col-xs-12 col-md-6"><input type="submit" value="<?php echo __("Opslaan");?>" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
				</div>
			<?php echo $this->Form->end(); ?>
		
		</div>
	</div>
</div>