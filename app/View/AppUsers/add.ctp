<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<?php echo $this->Form->create($model, array('role'=>'form') ); ?>
			<h2><?php echo __("Registreren");?></h2>
			<hr class="colorgraph">
			<div class="form-group">				
				<?php echo $this->Form->input('username', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Gebruikersnaam'), 'tabindex'=>'3')); ?>
			</div>
			<div class="form-group">				
				<?php echo $this->Form->input('hotel', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Hotel naam'), 'tabindex'=>'3')); ?>
			</div>
			<div class="form-group">				
				<?php echo $this->Form->input('city', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Plaats'), 'tabindex'=>'3')); ?>
			</div>
			<div class="form-group">				
				<?php echo $this->Form->input('function', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Functie'), 'tabindex'=>'3')); ?>
			</div>
			<div class="form-group">		
			<?php echo $this->Form->input('email', array('type'=>'email','label' => false,'class'=>'form-control input-lg', 'placeholder'=>'E-mail', 'tabindex'=>'4', 'error' => array('isValid' => __d('users', 'Must be a valid email address'),'isUnique' => __d('users', 'An account with that email already exists')))); ?>										
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">						
						<?php echo $this->Form->input('password', array('type'=>'password', 'label' => false, 'class'=>'form-control input-lg', 'placeholder'=> __("Wachtwoord"), 'tabindex'=>'5')); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<?php echo $this->Form->input('temppassword', array('type'=>'password', 'label' => false, 'class'=>'form-control input-lg', 'placeholder'=> __("Wachtwoord bevestigen"), 'tabindex'=>'6')); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7"><?php echo __("Akkoord");?></button>						
						<?php echo $this->Form->checkbox('approach', array('label' => false, 'checked'=>'checked', 'class'=>'hidden')); ?>                        
					</span>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-9">					 
					 <?php echo __("Met deze registratie gaat u akkoord dat u vrijblijvend benaderd mag worden voor informatie over de andere onderdelen van dit project");?>
				</div>
			</div>
			
			<div class="row mt30">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7"><?php echo __("Akkoord");?></button>						
						<?php echo $this->Form->checkbox('tos', array('label' => false, 'class'=>'hidden')); ?>
					</span>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-9">
					<?php echo __("Zodra u klikt op");?> <strong class="label label-primary"><?php echo __("Registreren");?></strong><?php echo __(", verklaart u zich akkoord met de toepasselijkheid van onze");?> <a href="#" data-toggle="modal" data-target="#t_and_c_m"><?php echo __("Disclaimer");?></a>					  
				</div>
			</div>
			
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6"><input type="submit" value="<?php echo __("Registreren");?>" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
				<div class="col-xs-12 col-md-6"><a href="#" class="btn btn-success btn-block btn-lg"><?php echo __("Inloggen");?></a></div>
			</div>
			<?php echo $this->Form->input('email_verified', array('type'=>'hidden', 'label' => false, 'value'=>'1')); ?>
		<?php echo $this->Form->end(); ?>
		
		<div class="well mt30">
			<?php echo __("Hotelschool The Hague gaat zorgvuldig om met uw gegevens. De gegevens die u verstrekt worden alleen voor het onderzoeksproject gebruikt. Uw anonimiteit is gewaarborgd en uw informatie wordt niet verspreid. Deze site maakt geen gebruik van cookies.");?>			
		</div>
		
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo __("Disclaimer");?></h4>
			</div>
			<div class="modal-body">
				<p>
				Er kunnen geen rechten aan de zelfscan worden ontleend. Hotelschool The Hague behoudt zich alle intellectuele eigendomsrechten en andere rechten voor met betrekking tot alle op of via deze website aangeboden informatie. Het is niet toegestaan informatie op deze website over te kopiëren, te downloaden of op enigerlei wijze openbaar te maken, te verspreiden of te verveelvoudigen zonder voorafgaande schriftelijke toestemming van Hotelschool The Hague. U mag informatie op deze website wel afdrukken en/of downloaden voor eigen persoonlijk gebruik. Hotelschool The Hague behoudt zich het recht voor de op of via deze website aangeboden informatie, met inbegrip van de tekst van deze disclaimer, te allen tijde te wijzigen zonder hiervan nadere aankondiging te doen. Het verdient aanbeveling periodiek na te gaan of de op of via deze website aangeboden informatie, met inbegrip van de tekst van deze disclaimer, is gewijzigd. Op deze website en de disclaimer is het Nederlands recht van toepassing. Alle geschillen uit hoofde van of in verband met deze disclaimer zullen bij uitsluiting worden voorgelegd aan de bevoegde rechter in Nederland. 
				</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>


<script>
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});
</script>