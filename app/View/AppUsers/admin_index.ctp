<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-11">
			<?php echo $this->Session->flash(); ?>
	        <div class="page-header">
	            <h2><?php echo __('Gebruikers'); ?></h2>
	        </div>

	        <table class="table table-bordered table-striped">
	            <thead>
	                <tr>
					<th><?php echo $this->Paginator->sort('username'); ?></th>
					<th><?php echo $this->Paginator->sort('email','E-mail'); ?></th>
					<th><?php echo $this->Paginator->sort('email_verified'); ?></th>
					<th><?php echo $this->Paginator->sort('active','Actief'); ?></th>
					<th><?php echo $this->Paginator->sort('is_admin'); ?></th>
					<th><?php echo $this->Paginator->sort('created','Aangemaakt op'); ?></th>
					<th class="actions"><?php echo __d('users', 'Actions'); ?></th>
	                </tr>
	            </thead>
	            <tbody>
	                <?php foreach ($users as $user): ?>
	                    <tr>
							<td>
								<?php echo $user[$model]['username']; ?>
							</td>
							<td>
								<?php echo $user[$model]['email']; ?>
							</td>
							<td>
								<?php echo $user[$model]['email_verified'] == 1 ? __d('users', 'Ja') : __d('users', 'Nee'); ?>
							</td>
							<td>
								<?php echo $user[$model]['active'] == 1 ? __d('users', 'Ja') : __d('users', 'Nee'); ?>
							</td>
							<td>
								<?php echo $user[$model]['is_admin'] == 1 ? __d('users', 'Ja') : __d('users', 'Nee'); ?>
							</td>
							<td>
								<?php echo $user[$model]['created']; ?>
							</td>
							<td class="actions">
								<?php echo $this->Html->link(__d('users', 'View'), array('action' => 'view', $user[$model]['id'])); ?>
								<?php echo $this->Html->link(__d('users', 'Edit'), array('action' => 'edit', $user[$model]['id'])); ?>
								<?php echo $this->Html->link(__d('users', 'Delete'), array('action' => 'delete', $user[$model]['id']), null, sprintf(__d('users', 'Are you sure you want to delete # %s?'), $user[$model]['id'])); ?>
							</td>
				                    </tr>
	                <?php endforeach; ?>
	            </tbody>
	        </table>
	        <div class="well">
	            <?php echo $this->element('pagination'); ?>
	            <?php echo $this->element('Users.paging'); ?>
	        </div>
	        <?php if ($this->Paginator->numbers()): ?>
	            <div class="pagination">
	                <ul>
	                    <?php echo $this->Paginator->first(); ?>
	                    <?php echo $this->Paginator->prev(); ?>
	                    <?php echo $this->Paginator->numbers(); ?>
	                    <?php echo $this->Paginator->next(); ?>
	                    <?php echo $this->Paginator->last(); ?>
	                </ul>
	            </div>
	        <?php endif; ?>
      	</div>
	</div>      	
</div>
