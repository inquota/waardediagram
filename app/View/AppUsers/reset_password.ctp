<div class="container" style="margin-top:30px">
	<div class="col-lg-6 col-md-offset-3">
		<div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo __d('users', 'Reset your password'); ?></h3>
            </div>
            <div class="panel-body">
            <?php echo $this->Session->flash('auth');?>
            	<?php echo $this->Form->create($model, array( 'action' => 'reset_password',$token, 'role'=>'form')); ?>
                    <fieldset>
                        <div class="form-group">                                    
                            <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'E-mail','type'=>'email','autofocus'=>'')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','placeholder'=>'Password','type'=>'password','autofocus'=>'')); ?>
                            <?php echo '<p>' . $this->Html->link(__d('users', 'I forgot my password'), array('action' => 'reset_password')) . '</p>'; ?>
                        </div>                                                                       
                        <?php echo $this->Form->button('Aanvragen', array('type' => 'submit', 'class'=> 'btn btn-sm btn-success')); ?>
                    </fieldset>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
	</div>
</div>