<div class="container" style="margin-top:30px">
	<div class="col-lg-6 col-md-offset-3">
		<div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo __d('users', 'Resend the Email Verification'); ?></h3>
            </div>
            <div class="panel-body">
			<p><?php echo __d('users', 'Please enter the email you used for registration and you\'ll get an email with further instructions.'); ?></p>
            <?php echo $this->Session->flash('auth');?>
            	<?php echo $this->Form->create($model, array('url' => array('admin' => false,'action' => 'resend_verification','role'=>'form'))); ?>
                    <fieldset>
                        <div class="form-group">                                    
                            <?php echo $this->Form->input('email', array('label'=>false,'class'=>'form-control','placeholder'=>'E-mail','type'=>'email','autofocus'=>'')); ?>
                        </div>                                                                     
                        <?php echo $this->Form->button('Aanvragen', array('type' => 'submit', 'class'=> 'btn btn-sm btn-success')); ?>
                    </fieldset>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
	</div>
</div>