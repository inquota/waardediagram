<div class="container">
	<div class="row">
	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<?php echo $this->Form->create($model, array('role'=>'form') ); ?>
				<h2><?php echo __("Bewerken");?></h2>
				<hr class="colorgraph">
				<div class="form-group">		
					<label><?php echo __("Gebruikersnaam");?></label>		
					<?php echo $this->Form->input('username', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>'Hotelnaam', 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">
					<label><?php echo __("Hotel naam");?></label>				
					<?php echo $this->Form->input('hotel', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Hotel'), 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">
					<label><?php echo __("Plaats");?></label>				
					<?php echo $this->Form->input('city', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Plaats'), 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">		
					<label><?php echo __("Functie");?></label>		
					<?php echo $this->Form->input('function', array('label' => false, 'class'=>'form-control input-lg', 'placeholder'=>__('Functie'), 'tabindex'=>'3')); ?>
				</div>
				<div class="form-group">		
					<label><?php echo __("E-mail");?></label>
				<?php echo $this->Form->input('email', array('type'=>'email','label' => false,'class'=>'form-control input-lg', 'placeholder'=>'E-mail', 'tabindex'=>'4', 'error' => array('isValid' => __d('users', 'Must be a valid email address'),'isUnique' => __d('users', 'An account with that email already exists')))); ?>										
				</div>
			
				<hr class="colorgraph">
				<div class="row" style="padding-bottom: 20px;">
					<div class="col-xs-12 col-md-6"><input type="submit" value="<?php echo __("Opslaan");?>" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
				</div>
			<?php echo $this->Form->end(); ?>
		
		</div>
	</div>
</div>