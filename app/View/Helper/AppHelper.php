<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
	public $helpers=array('Number');
	/**
	 * GenerateDropDownKenmerken
	 * @param Arrray $aPercent_negative
	 * @param Array $aPercent_positive
	 * @param Integer $hotel
	 * @param Integer $k
	 * @param Array $session2
	 * @param Array $kenmerken_merged
	 */
	public function GenerateDropDownKenmerken($aPercent_negative,$aPercent_positive,$hotel,$k,$session2,$kenmerken_merged=null){
		// This is a check for editing this page
		if($kenmerken_merged==null && !$session2) : ?>
			<select style="display: none;" name="kenmerken_pos_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_neg_<?php echo $hotel;?>_<?php echo $k; ?>">
	    		<option value=""><?php echo __("kies");?></option>
	    		<option value="0"><?php echo __("0% (zelfde)");?></option>
	    		<?php foreach($aPercent_negative as $percent_negative): ?>
	    		<option value="<?php echo $percent_negative; ?>"><?php echo $percent_negative; ?>%</option>
	    		<?php endforeach; ?>
	    	</select>
	    	
	    	<select style="display: none;" name="kenmerken_neg_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_pos_<?php echo $hotel;?>_<?php echo $k; ?>">
	    		<option value=""><?php echo __("kies");?></option>
	    		<option value="0"><?php echo __("0% (zelfde)");?></option>
	    		<?php foreach($aPercent_positive as $percent_positive): ?>
	    		<option value="<?php echo $percent_positive; ?>"><?php echo $percent_positive; ?>%</option>
	    		<?php endforeach; ?>
	    	</select>
	    	
		<?php else :
			$sign=substr($kenmerken_merged[$k], 0,1);
			if($session2) :
				$page=$session2['Page'];
				$choosen_option=$page[$hotel.'_kenmerk'.($k+1)];
			endif;

			if($sign=='+') :
			?>			
				<select name="kenmerken_pos_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_neg_<?php echo $hotel;?>_<?php echo $k; ?>">
		    		<option value="<?php echo $kenmerken_merged[$k];?>"><?php echo $kenmerken_merged[$k];?>%</option>
		    		<option value="0"><?php echo __("0% (zelfde)");?></option>
		    		<?php foreach($aPercent_negative as $percent_negative): ?>
		    		<option value="<?php echo $percent_negative; ?>"><?php echo $percent_negative; ?>%</option>
		    		<?php endforeach; ?>
		    	</select>
			<?php elseif($sign=='-') : ?>
				<select name="kenmerken_neg_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_pos_<?php echo $hotel;?>_<?php echo $k; ?>">
		    		<option value="<?php echo $kenmerken_merged[$k];?>"><?php echo $kenmerken_merged[$k];?>%</option>
		    		<option value="0"><?php echo __("0% (zelfde)");?></option>
		    		<?php foreach($aPercent_positive as $percent_positive): ?>
		    		<option value="<?php echo $percent_positive; ?>"><?php echo $percent_positive; ?>%</option>
		    		<?php endforeach; ?>
		    	</select>	
			<?php elseif($sign==0) : ?>	
				<?php if($choosen_option==1) : ?>
					<select name="kenmerken_neg_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_pos_<?php echo $hotel;?>_<?php echo $k; ?>">
			    		<option value="<?php echo $kenmerken_merged[$k];?>"><?php echo $kenmerken_merged[$k];?>%</option>
			    		<option value="0"><?php echo __("0% (zelfde)");?></option>
			    		<?php foreach($aPercent_positive as $percent_positive): ?>
			    		<option value="<?php echo $percent_positive; ?>"><?php echo $percent_positive; ?>%</option>
			    		<?php endforeach; ?>
			    	</select>	
				<?php elseif($choosen_option==3) : ?>
					<select name="kenmerken_pos_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_neg_<?php echo $hotel;?>_<?php echo $k; ?>">
			    		<option value="<?php echo $kenmerken_merged[$k];?>"><?php echo $kenmerken_merged[$k];?>%</option>
			    		<option value="0"><?php echo __("0% (zelfde)");?></option>
			    		<?php foreach($aPercent_negative as $percent_negative): ?>
			    		<option value="<?php echo $percent_negative; ?>"><?php echo $percent_negative; ?>%</option>
			    		<?php endforeach; ?>
			    	</select>
				<?php endif; ?>
			<?php elseif($kenmerken_merged==null || $kenmerken_merged=false) : ?>
				<select style="display: none;" name="kenmerken_pos_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_neg_<?php echo $hotel;?>_<?php echo $k; ?>">
		    		<option value=""><?php echo __("kies");?></option>
		    		<option value="0"><?php echo __("0% (zelfde)");?></option>
		    		<?php foreach($aPercent_negative as $percent_negative): ?>
		    		<option value="<?php echo $percent_negative; ?>"><?php echo $percent_negative; ?>%</option>
		    		<?php endforeach; ?>
		    	</select>
		    	
		    	<select style="display: none;" name="kenmerken_neg_<?php echo $hotel;?>[]" class="col-md-4 form-control" id="kk_percentage_pos_<?php echo $hotel;?>_<?php echo $k; ?>">
		    		<option value=""><?php echo __("kies");?></option>
		    		<option value="0"><?php echo __("0% (zelfde)");?></option>
		    		<?php foreach($aPercent_positive as $percent_positive): ?>
		    		<option value="<?php echo $percent_positive; ?>"><?php echo $percent_positive; ?>%</option>
		    		<?php endforeach; ?>
		    	</select>		
			<?php endif; ?>
		<?php endif; ?>
		<?php
	}
	
	/**
	 * GenerateDropDownWeight
	 * @param Array $aPercent_negative
	 * @param Array $aPercent_positive
	 * @param Integer $k
	 */
	public function GenerateDropDownWeight($aPercent_negative,$aPercent_positive,$k){
		?>
		<select style="display:none;" name="dropdown_weight[]" class="col-md-2 form-control" id="weight_percentage_neg_<?php echo $k; ?>">
    		<option value=""><?php echo __("kies");?></option>
    		<?php foreach($aPercent_negative as $percent_negative): ?>
    		<option value="<?php echo $percent_negative; ?>"><?php echo $percent_negative; ?>%</option>
    		<?php endforeach; ?>
    	</select>
    	
		<input type="text" value="0" style="display:none;" name="dropdown_weight[]" class="col-xs-1 form-control" id="weight_percentage_zero_<?php echo $k; ?>">
    	
    	<select style="display:none;" name="dropdown_weight[]" class="col-md-2 form-control" id="weight_percentage_pos_<?php echo $k; ?>">
    		<option value=""><?php echo __("kies");?></option>
    		<?php foreach($aPercent_positive as $percent_positive): ?>
    		<option value="<?php echo $percent_positive; ?>"><?php echo $percent_positive; ?>%</option>
    		<?php endforeach; ?>
    	</select>
		<?php
	}
	
	/**
	 * GenerateDropdownTo
	 * Generate a dropdown menu on step 3 for 'wegingsfactoren'
	 * @param Integer $k
	 * @param Array $session3
	 */
	public function GenerateDropdownTo($k,$session3){
		// If session isset we used the session vars
		if($session3) :
		?>
			<select name="dropdown_weight2[]" class="col-xs-2 form-control to_count" id="weight_percentage2_<?php echo $k; ?>">
	    		<option value="<?php echo $session3['dropdown_weight2'][$k]; ?>"><?php echo $session3['dropdown_weight2'][$k]; ?>%</option>
	    		<?php for($i=1;$i<21;$i++): ?>
	    			<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
	    		<?php endfor; ?>
	    	</select>
		<?php else : ?>
			<select style="display:none;" name="dropdown_weight2[]" class="col-xs-2 form-control to_count" id="weight_percentage2_<?php echo $k; ?>">
	    		<option value=""><?php echo __("kies");?></option>
	    		<?php for($i=1;$i<21;$i++): ?>
	    			<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
	    		<?php endfor; ?>
	    	</select>
    	<?php endif; ?>
		<?php 
	}
	/**
	 * CoordsNetto
	 * @param Array $CoordsxAxis
	 * @param Array $CoordsyAxis
	 * @return Array $r
	 */
	public function CoordsNetto($CoordsxAxis,$CoordsyAxis)
	{
		$r=array();
		foreach($CoordsxAxis as $k=>$v)
		{
			$r[]= ($v - $CoordsyAxis[$k]);
		}
		return $r;
	}
	
	/**
	 * GenerateSeriesData
	 * @param Decimal $val1
	 * @param Decimal $val2
	 * @param Decimal $val3
	 * @param String $hotel
	 */
	public function GenerateSeriesData($val1,$val2,$val3){
		$hotel=array($val1,$val2,$val3);
		$hotel=implode(',',$hotel);
		return $hotel;
	}
	
	/**
	 * GetBeterOfSlechter
	 * @param Decimal $input
	 * @return String
	 */
	public function GetBeterOfSlechter($input){
		if( $input > 0 )
		{
			return "<span class='label label-success'>".__("Beter")."</span>";
		}else{
			return "<span class='label label-danger'>".__("Slechter")."</span>";
		}
	}
	
	/**
	 * GetHotelBackgroundColor
	 * @param Integer $key
	 * @param String $background_color
	 */
	public function GetHotelBackgroundColor($key){
		switch ($key) {
			case 0:
				$background_color='rgba(67,67,72,0.9)';
				break;
				
			case 1:
				$background_color='rgba(144,237,125,0.9)';
				break;
				
			case 2:
				$background_color='rgba(247,163,92,0.9)';
				break;
				
			case 3:
				$background_color='rgba(128,133,233,0.9)';
				break;
				
			case 4:
				$background_color='rgba(241,92,128,0.9)';
				break;
			
			default:
				
				break;
		}
		return $background_color;
	}
	
	/**
	 * GetRanking
	 * @param Array $array
	 * @return Array $newArray
	 */
	public function GetRanking($array){		
		sort($array,SORT_NUMERIC);
		$array = array_reverse($array);
		
		$newArray = array();
		foreach ($array as $key => $val) {
			$newKey = (string) $val;
			$newArray[$newKey] = ($key +1);
		}
		return $newArray;
	}
	
	public function Substract($var, $lengte) {
		$ret = $var;
		if (strlen($ret) > $lengte) {
		$ret = substr($ret, 0, $lengte-3)."...";
		}
		return $ret;
	}
}