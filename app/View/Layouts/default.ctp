<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'Marktpositie analyse tool');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<!-- CSS -->
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
	
	<?php echo $this->Html->css('style'); ?>
	<?php echo $this->Html->css('results.breadcrumb'); ?>
	
	<link href="/css/flag-icon-css/assets/docs.css" rel="stylesheet">
	<link href="/css/flag-icon-css/css/flag-icon.css" rel="stylesheet">
	
	<!-- CSS External -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:700' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />
	
	
	<!-- JS -->
	<script src="/js/jquery.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/highcharts-more.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>	
	<script src="//cdn.ckeditor.com/4.4.4/standard/ckeditor.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/jquery.knob.1.2.0.js" type="text/javascript" language="JavaScript1.2"></script>
	
	
</head>
<body>
	<?php if(isset($old_browser_detected) && $old_browser_detected == 1) : ?>
		<p class="bg-warning">			
			<?php echo __("Helaas wordt uw browser niet ondersteunt door de Marktpositie Analyse. Wij verzoeken u om gebruik te maken van Firefox, Chrome, Internet Explorer 9 of hoger.");?>
		</p>
	<?php else : ?>
	<div id="wrap">		
		<?php if($is_admin) : ?>
			<?php echo $this->element('admin_menu'); ?>
		<?php else : ?>
			<?php echo $this->element('menu'); ?>
		<?php endif; ?>

		<div id="content">

			<div class="container">
				<div class="row">
			      <div class="col-xs-12 col-sm-6 col-lg-11">
					<?php echo $this->Session->flash(); ?>
				 </div>
				</div>
			</div>

			<?php echo $this->fetch('content'); ?>
		</div>
<?php echo $this->element('footer'); ?>
<?php endif; ?>