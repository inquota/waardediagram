<div class="container">
	<div class="row">
		
      <div class="col-md-offset-1 col-xs-12 col-sm-10 col-lg-9 default-block">
        <?php echo utf8_decode($blocks[1]['Text']['tekst']); ?>
      </div>
      
       <div class="col-md-offset-1 col-xs-12 col-sm-10 col-lg-9 well well-sm mt30">
        <?php echo utf8_decode($blocks[2]['Text']['tekst']); ?>
      </div>
      
        <div class="col-md-offset-1 col-xs-12 col-sm-10 col-lg-9 mt30">
			<a href="/hoe-werkt-het/" class="btn btn-primary col-md-4 col-md-offset-4"><?php echo __("Ga verder");?></a>	
       </div> 
      
     </div>
</div>